<?php
/**
 * Created by PhpStorm.
 * User: Nikola
 * Date: 03-Apr-19
 * Time: 18:03
 */

namespace App;


class Period
{
    public $datum1;
    public $datum2;
    public $izvestaji;

    /**
     * Period constructor.
     * @param $datum1
     */
    public function __construct($datum1)
    {
        $this->datum1 = $datum1;
        $this->izvestaji = [];
    }

    /**
     * @return mixed
     */
    public function getDatum1()
    {
        return $this->datum1;
    }

    /**
     * @param mixed $datum1
     */
    public function setDatum1($datum1): void
    {
        $this->datum1 = $datum1;
    }

    /**
     * @return mixed
     */
    public function getDatum2()
    {
        return $this->datum2;
    }

    /**
     * @param mixed $datum2
     */
    public function setDatum2($datum2): void
    {
        $this->datum2 = $datum2;
    }


    /**
     * @return array
     */
    public function getIzvestaji(): array
    {
        return $this->izvestaji;
    }

    /**
     * @param array $izvestaji
     */
    public function setIzvestaji(array $izvestaji): void
    {
        $this->izvestaji = $izvestaji;
    }




}