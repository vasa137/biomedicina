<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrupaUsluga extends Model
{
    protected $table = 'grupa_usluga';
    protected $fillable = ['naziv', 'opis'];

    protected $appends = ['usluge'];

    private $usluge;

    public function setUslugeAttribute($usluge){
        $this->usluge = $usluge;
    }

    public function getUslugeAttribute(){
        return $this->usluge;
    }

    public static function dohvatiSve(){
        return GrupaUsluga::all();
    }
}
