<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rezervacija extends Model
{
    protected $table = 'rezervacija';
    protected $fillable = [ 'klijent', 'telefon', 'email', 'napomena', 'status', 'vreme_pocetka', 'vreme_kraja', 'id_usluga', 'id_user', 'admin_kreirao', 'sakriven'];

    protected $appends = ['usluga'];

    protected $usluga;

    public function setUslugaAttribute($usluga){
        $this->usluga = $usluga;
    }

    public function getUslugaAttribute(){
        return $this->usluga;
    }

    public static function dohvatiSaId($id){
        return Rezervacija::where('id', $id)->first();
    }

    public static function dohvatiSvePotvrdjene(){
        return Rezervacija::where('sakriven', 0)->where('status', 'potvrdjena')->get();
    }

    public static function dohvatiSveNaCekanju(){
        return Rezervacija::where('sakriven', 0)->where('status', 'na_cekanju')->get();
    }

    public static function dohvatiSveObrisaneOdbijene(){
        return Rezervacija::where('sakriven', 1)->orWhere('status', 'odbijena')->get();
    }


    public function napuni($klijent, $telefon, $email, $napomena, $vreme_pocetka, $vreme_kraja, $admin_kreirao, $id_usluga, $id_user, $status){
        $this->klijent = $klijent;
        $this->telefon = $telefon;
        $this->email = $email;
        $this->napomena = $napomena;
        $this->vreme_pocetka = $vreme_pocetka;
        $this->vreme_kraja = $vreme_kraja;
        $this->admin_kreirao = $admin_kreirao;
        $this->id_usluga = $id_usluga;
        $this->id_user = $id_user;
        $this->status = $status;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }

    public function potvrdi($vreme_pocetka, $vreme_kraja){
        $this->vreme_pocetka = $vreme_pocetka;
        $this->vreme_kraja = $vreme_kraja;
        $this->status = 'potvrdjena';

        $this->save();
    }

    public function promeniStatus($status){
        $this->status = $status;

        $this->save();
    }

    public static function dohvatiBrojRezervacijaZaUslugu($id){
        return Rezervacija::where('id_usluga', $id)->count();
    }

    public static function dohvatiZaKlijenta($id){
        return Rezervacija::where('id_user', $id)->get();
    }

    public static function dohvatiRezervacijeKojeSePreklapaju($vreme_pocetka){
        return Rezervacija::where('status', 'potvrdjena')->where('vreme_pocetka', '<=', $vreme_pocetka)->where('vreme_kraja', '>', $vreme_pocetka)->get();
    }
}
