<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usluga extends Model
{
    protected $table = 'usluga';
    protected $fillable = ['naziv', 'opis', 'iznos', 'sakriven', 'id_grupa_usluga'];

    protected $appends = ['broj_rezervacija'];

    private $broj_rezervacija;

    public function setBrojRezervacijaAttribute($broj_rezervacija){
        $this->broj_rezervacija = $broj_rezervacija;
    }

    public function getBrojRezervacijaAttribute(){
        return $this->broj_rezervacija;
    }

    public static function dohvatiSaId($id){
        return Usluga::where('id',$id)->first();
    }

    public static function dohvatiSveAktivne(){
        return Usluga::where('sakriven', 0)->get();
    }

    public static function dohvatiSve(){
        return Usluga::all();
    }

    public function napuni($naziv, $opis, $id_grupa_usluga, $iznos){
        $this->naziv = $naziv;
        $this->opis = $opis;
        $this->id_grupa_usluga = $id_grupa_usluga;
        $this->iznos = $iznos;

        $this->save();
    }

    public function obrisi(){
        $this->sakriven = 1;

        $this->save();
    }

    public function restauriraj(){
        $this->sakriven = 0;

        $this->save();
    }
}
