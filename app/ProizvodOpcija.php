<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProizvodOpcija extends Model
{
    protected $table = 'proizvod_opcija';

    protected $fillable = ['id_proizvod', 'id_opcija', 'sifra', 'cena', 'na_popustu', 'cena_popust',
        'lager', 'br_komada', 'nabavna_cena', 'ima_slike'];

    protected $appends = ['grupa_opcija'];

    protected $grupa_opcija;

    public function getGrupaOpcijaAttribute(){
        return $this->grupa_opcija;
    }

    public function setGrupaOpcijaAttribute($grupaOpcija){
        $this->grupa_opcija = $grupaOpcija;
    }

    public function napuni($id_proizvod, $id_opcija, $sifra, $cena, $na_popustu, $cena_popust, $lager, $br_komada, $nabavna_cena, $ima_slike){
        $this->id_proizvod = $id_proizvod;
        $this->id_opcija =$id_opcija;
        $this->sifra = $sifra;
        $this->cena = $cena;
        $this->na_popustu = $na_popustu;
        $this->cena_popust = $cena_popust;
        $this->lager =$lager;
        $this->br_komada =$br_komada;
        $this->nabavna_cena = $nabavna_cena;
        $this->ima_slike = $ima_slike;

        $this->save();
    }

    public static function dohvatiOpcijeZaProizvod($id){
        return ProizvodOpcija::where('id_proizvod', $id)->get();
    }

    public static function obrisiOpcijeZaProizvod($id){
        ProizvodOpcija::where('id_proizvod', $id)->delete();
    }

    public static function dohvatiBrojProizvodaZaOpciju($id){
        return DB::select("
            select IFNULL(COUNT(p.id), 0) as broj_proizvoda
            FROM proizvod_opcija po, proizvod p
            WHERE id_opcija = $id
            AND po.id_proizvod = p.id
            AND p.sakriven = 0
        ")[0]->broj_proizvoda;
    }
}
