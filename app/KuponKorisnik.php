<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KuponKorisnik extends Model
{
    protected $table = 'kupon_korisnik';

    protected $fillable = ['id_kupon', 'id_user'];

    public static function dohvatiKorisnikeZaKupon($id){
        return KuponKorisnik::where('id_kupon', $id)->get();
    }

    public function napuni($id_kupon, $id_user){
        $this->id_kupon = $id_kupon;
        $this->id_user = $id_user;

        $this->save();
    }

    public static function obrisiKorisnikeZaKupon($id){
        KuponKorisnik::where('id_kupon', $id)->delete();
    }
}
