<?php

namespace App\Http\Controllers;

use App\Kupon;
use App\Porudzbina;
use App\PorudzbinaVaucer;
use App\Proizvod;
use App\Rezervacija;
use App\StavkaPorudzbina;
use App\Usluga;
use App\Vaucer;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Hash;
use Session;
use Carbon\Carbon;
use Mail;

class klijentKorisnikController extends Controller
{
    private function popuniUsluguURezervaciji($rezervacija){
        $rezervacija->usluga = Usluga::dohvatiSaId($rezervacija->id_usluga);
    }

    private function formirajPorudzbinu($porudzbina){
        $id = $porudzbina->id;

        $porudzbina->stavke = StavkaPorudzbina::dohvatiStavkeZaPorudzbinu($id);

        if($porudzbina->ima_vaucere){
            $porudzbinaVauceri = PorudzbinaVaucer::dohvatiVaucereZaPorudzbinu($id);

            $vauceri = [];

            foreach($porudzbinaVauceri as $porudzbinaVaucer){
                $vauceri [] = Vaucer::dohvatiSaId($porudzbinaVaucer->id_vaucer);
            }

            $porudzbina->vauceri = $vauceri;
        }

        foreach($porudzbina->stavke as $stavka){
            $stavka->proizvod = Proizvod::dohvatiSaId($stavka->id_proizvod);

            if($stavka->id_kupon != null){
                $stavka->kupon = Kupon::dohvatiSaId($stavka->id_kupon);
            }
        }

        return $porudzbina;
    }
    public function nalog(){
        if(Auth::check()){
            $user = Auth::user();

            if($user->admin){
                return redirect('/admin');
            } else{
                $user = Auth::user();

                $porudzbine = Porudzbina::dohvatiZaKupca($user->id);

                foreach($porudzbine as $porudzbina){
                    $this->formirajPorudzbinu($porudzbina);
                }

                $rezervacije = Rezervacija::dohvatiZaKlijenta($user->id);

                foreach($rezervacije as $rezervacija){
                    $this->popuniUsluguURezervaciji($rezervacija);
                }

                return view('nalog', compact('user', 'porudzbine', 'rezervacije'));
            }
        } else{
            return redirect('/login');
        }
    }


    public function kontaktiraj(Request $request)
    {
        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme

        ];



         Mail::send('mailovi.kontakt', $data, function($message) use ($ime_prezime) {
         $message->to('vasa.nikola@biomedicina.varius-soft.com', 'Poruka sa sajta- '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('vasa.nikola@biomedicina.varius-soft.com' ,$ime_prezime.' - Poruka sa sajta');
        });


        return redirect('/kontakt-uspesan');
    }


    public function promeni_licne_podatke(Request $request){
        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'nullable|string|max:19',
            'grad' => 'nullable|string|max:254',
            'adresa' => 'nullable|string|max:254',
            'zip' => 'nullable|numeric|min:10000|max:99999',
        ]);

        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $grad = $_POST['grad'];
        $adresa = $_POST['adresa'];
        $zip = $_POST['zip'];

        $user = Auth::user();

        $user->promeniLicnePodatke($ime_prezime, $telefon, $grad, $adresa, $zip);

        Session::flash('success', 'Uspešno ste promenili lične podatke.');
        return Redirect::back();
    }

    public function promeni_lozinku(Request $request){
        $request->validate([
            'password' =>  'required|string|min:6|confirmed|max:254'
        ]);

        $password = $_POST['password'];

        $user = Auth::user();

        if(!Hash::check( $password, $user->password)) {
            $hashPassword = Hash::make($password);

            $user->promeniLozinku($hashPassword);
            Session::flash('success', 'Uspešno ste promenili lozinku.');
            return Redirect::back();
        } else{
            return Redirect::back()->withErrors(['greska' => 'Postavili ste već postojeću lozinku.']);
        }
    }

    public function rezervacija(){
        $usluge = Usluga::dohvatiSveAktivne();

        return view('rezervisi', compact('usluge'));
    }

    public function rezervisi(Request $request){
        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'email' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'id_usluga' => 'required|numeric',
            'datum' => 'required|date_format:Y-m-d',
            'vreme_pocetka' => 'required|numeric|min:480|max:1170',
            'napomena' => 'nullable|string'
        ]);

        $ime_prezime = $_POST['ime_prezime'];
        $email = $_POST['email'];
        $telefon = $_POST['telefon'];
        $id_usluga = $_POST['id_usluga'];
        $datum = $_POST['datum'];
        $vreme_pocetka = $_POST['vreme_pocetka'];
        $napomena = $_POST['napomena'];

        $usluga = Usluga::dohvatiSaId($id_usluga);

        $vreme_h = intval($vreme_pocetka / 60);
        $vreme_m = $vreme_pocetka % 60;

        $vreme = date('Y-m-d H:i:s', strtotime("$datum $vreme_h:$vreme_m"));

        if($usluga->sakriven || strtotime("$datum $vreme_h:$vreme_m") - time() < 0){
            abort(404);
        }

        $id_user = null;

        if(Auth::check()){
            $id_user = Auth::user()->id;
        }

        $rezervacija = new Rezervacija();

        $status = 'na_cekanju';

        $rezervacija->napuni($ime_prezime, $telefon, $email, $napomena, $vreme, null, 0, $id_usluga, $id_user, $status);

        $data =[
            'ime_prezime' => $ime_prezime,
            'telefon' =>  $telefon,
            'napomena' => $napomena,
            'email' => $email,
            'vreme' => $vreme,
            'usluga' => $usluga->naziv
        ];



        Mail::send('mailovi.rezervacija', $data, function($message) use ($ime_prezime) {
            $message->to('vasa.nikola@biomedicina.varius-soft.com', 'Rezervacija sa sajta- '.$ime_prezime)->subject('Rezervacija sa sajta- '.$ime_prezime);
            $message->from('vasa.nikola@biomedicina.varius-soft.com',$ime_prezime.' - Rezervacija sa sajta');
        });

        return redirect('/rezervacija-uspesna');
    }

    public function proveri_rezervacije(){
        $datum = $_GET['datum'];
        $vreme_pocetka = $_GET['vreme_pocetka'];

        $vreme_h = intval($vreme_pocetka / 60);
        $vreme_m = $vreme_pocetka % 60;

        $vreme = date('Y-m-d H:i:s', strtotime("$datum $vreme_h:$vreme_m"));

        if(count(Rezervacija::dohvatiRezervacijeKojeSePreklapaju($vreme)) == 0){
            return 1;
        } else{
            return 0;
        }
    }
}
