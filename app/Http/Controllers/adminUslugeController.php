<?php

namespace App\Http\Controllers;

use App\GrupaUsluga;
use App\Rezervacija;
use App\Usluga;
use Illuminate\Http\Request;
use Redirect;
class adminUslugeController extends Controller
{
    private function popuniUslugaInfo($usluga){
        $usluga->broj_rezervacija = Rezervacija::dohvatiBrojRezervacijaZaUslugu($usluga->id);
    }

    public function usluga($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $grupeUsluga = GrupaUsluga::dohvatiSve();

        if(!$izmena){
            return view('admin.adminUsluga', compact('izmena', 'grupeUsluga'));
        } else{
            $usluga = Usluga::dohvatiSaId($id);

            if($usluga == null){
                abort(404);
            }

            $this->popuniUslugaInfo($usluga);

            return view('admin.adminUsluga', compact('izmena', 'usluga', 'grupeUsluga'));
        }
    }

    public function usluge(){
        $usluge = Usluga::dohvatiSve();

        $grupeUsluga = GrupaUsluga::dohvatiSve();

        $uslugePoGrupama = [];

        $brojAktivnihUsluga = 0;

        foreach($grupeUsluga as $grupaUsluga){
            $uslugePoGrupama[$grupaUsluga->id] = [];
        }

        foreach($usluge as $usluga){
            $this->popuniUslugaInfo($usluga);
            $uslugePoGrupama[$usluga->id_grupa_usluga][] = $usluga;

            if(!$usluga->sakriven){
                $brojAktivnihUsluga++;
            }
        }

        foreach($grupeUsluga as $grupaUsluga){
            $grupaUsluga->usluge = $uslugePoGrupama[$grupaUsluga->id];
        }

        return view('admin.adminUsluge', compact('usluge', 'grupeUsluga', 'brojAktivnihUsluga'));
    }

    public function sacuvaj_uslugu($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $naziv = $_POST['naziv'];
        $opis = $_POST['opis'];
        $iznos = $_POST['iznos'];
        $id_grupa_usluga = $_POST['grupaUsluga'];

        $zaPunjenje = true;

        if($izmena){
            $usluga = Usluga::dohvatiSaId($id);

            if($usluga->naziv == $naziv && $usluga->opis == $opis && $usluga->id_grupa_usluga == $id_grupa_usluga && $usluga->iznos == $iznos){
                $zaPunjenje = false;
            }

        } else{
            $usluga = new Usluga();
        }

        if($zaPunjenje) {
            $usluga->napuni($naziv, $opis, $id_grupa_usluga, $iznos);
        }

        return redirect('/admin/usluga/' . $usluga->id);
    }

    public function obrisi_uslugu($id){
        $usluga = Usluga::dohvatiSaId($id);

        $usluga->obrisi();

        return Redirect::back();
    }

    public function restauriraj_uslugu($id){
        $usluga = Usluga::dohvatiSaId($id);

        $usluga->restauriraj();

        return Redirect::back();
    }
}
