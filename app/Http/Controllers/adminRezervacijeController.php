<?php

namespace App\Http\Controllers;

use App\Rezervacija;
use App\Usluga;
use Illuminate\Http\Request;
use Auth;
use DateTime;
use DateTimeZone;
use Redirect;
class adminRezervacijeController extends Controller
{
    private function popuniUsluguURezervaciji($rezervacija){
        $rezervacija->usluga = Usluga::dohvatiSaId($rezervacija->id_usluga);
    }

    public function rezervacije(){
        $potvrdjeneRezervacije = Rezervacija::dohvatiSvePotvrdjene();

        $rezervacijeNaCekanju = Rezervacija::dohvatiSveNaCekanju();

        $obrisaneOdbijeneRezervacije = Rezervacija::dohvatiSveObrisaneOdbijene();


        foreach($potvrdjeneRezervacije as $rezervacija){
            $this->popuniUsluguURezervaciji($rezervacija);
        }


        foreach($rezervacijeNaCekanju as $rezervacija){
            $this->popuniUsluguURezervaciji($rezervacija);
        }


        foreach($obrisaneOdbijeneRezervacije as $rezervacija){
            $this->popuniUsluguURezervaciji($rezervacija);
        }


        return view('admin.adminRezervacije', compact('potvrdjeneRezervacije', 'rezervacijeNaCekanju', 'obrisaneOdbijeneRezervacije'));
    }

    public function rezervacija($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $usluge = Usluga::dohvatiSveAktivne();

        if($izmena){
            $rezervacija = Rezervacija::dohvatiSaId($id);

            $rezervacija->vreme_pocetka = new DateTime($rezervacija->vreme_pocetka, new DateTimeZone('Europe/Belgrade'));//date('Y-m-d H:i:s', strtotime($rezervacija->vreme_pocetka));

            if($rezervacija->vreme_kraja != null) {
                $rezervacija->vreme_kraja = new DateTime($rezervacija->vreme_kraja, new DateTimeZone('Europe/Belgrade'));//date('Y-m-d H:i:s', strtotime($rezervacija->vreme_kraja));
            }

            $this->popuniUsluguURezervaciji($rezervacija);

            return view('admin.adminRezervacija', compact('izmena', 'rezervacija', 'usluge'));
        } else {


            return view('admin.adminRezervacija', compact('izmena', 'usluge'));
        }
    }

    public function sacuvaj_rezervaciju($id){
        $izmena = false;

        if($id > 0){
            $izmena = true;
        }

        $id_usluga = $_POST['id_usluga'];
        $klijent = $_POST['klijent'];
        $telefon = $_POST['telefon'];
        $email = $_POST['email'];
        $napomena = $_POST['napomena'];

        $datum = $_POST['datum'];

        $vreme_pocetka_h = $_POST['vreme_pocetka_h'];
        $vreme_pocetka_m = $_POST['vreme_pocetka_m'];

        $vreme_kraja_h = $_POST['vreme_kraja_h'];
        $vreme_kraja_m = $_POST['vreme_kraja_m'];

        $vreme_pocetka = date('Y-m-d H:i', strtotime("$datum $vreme_pocetka_h:$vreme_pocetka_m"));
        $vreme_kraja = date('Y-m-d H:i', strtotime("$datum $vreme_kraja_h:$vreme_kraja_m"));

        $admin_kreirao = true;

        $id_user = Auth::user()->id;

        $status = 'potvrdjena';

        if($izmena){
            $rezervacija = Rezervacija::dohvatiSaId($id);
        } else{
            $rezervacija = new Rezervacija();
        }

        $rezervacija->napuni($klijent, $telefon, $email, $napomena, $vreme_pocetka, $vreme_kraja, $admin_kreirao, $id_usluga, $id_user, $status);

        return redirect('/admin/rezervacija/' . $rezervacija->id);
    }

    public function obrisi_rezervaciju($id){
        $rezervacija = Rezervacija::dohvatiSaId($id);

        $rezervacija->obrisi();

        return Redirect::back();
    }

    public function restauriraj_rezervaciju($id){
        $rezervacija = Rezervacija::dohvatiSaId($id);

        $rezervacija->restauriraj();

        return Redirect::back();
    }

    public function potvrdi_rezervaciju($id){
        $datum = $_POST['datum'];

        $vreme_pocetka_h = $_POST['vreme_pocetka_h'];
        $vreme_pocetka_m = $_POST['vreme_pocetka_m'];

        $vreme_kraja_h = $_POST['vreme_kraja_h'];
        $vreme_kraja_m = $_POST['vreme_kraja_m'];

        $vreme_pocetka = date('Y-m-d H:i', strtotime("$datum $vreme_pocetka_h:$vreme_pocetka_m"));
        $vreme_kraja = date('Y-m-d H:i', strtotime("$datum $vreme_kraja_h:$vreme_kraja_m"));

        $rezervacija = Rezervacija::dohvatiSaId($id);

        $rezervacija->potvrdi($vreme_pocetka, $vreme_kraja);

        return Redirect::back();
    }

    public function status_rezervacije($id, $status){
        $rezervacija = Rezervacija::dohvatiSaId($id);

        $rezervacija->promeniStatus($status);

        return Redirect::back();
    }
}
