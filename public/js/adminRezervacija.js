function validirajVreme(){
    let vreme_pocetka_h = parseInt(document.getElementById('vreme_pocetka_h').value);
    let vreme_pocetka_m = parseInt(document.getElementById('vreme_pocetka_m').value);

    let vreme_kraja_h = parseInt(document.getElementById('vreme_kraja_h').value);
    let vreme_kraja_m = parseInt(document.getElementById('vreme_kraja_m').value);

    let errorMessage = document.getElementById('errorMessage');

    errorMessage.innerHTML = '';
    if(vreme_pocetka_h * 60 + vreme_pocetka_m > vreme_kraja_h * 60 + vreme_kraja_m){
        errorMessage.innerHTML = 'Vreme početka mora biti pre vremena kraja';
        return false;
    }
    else{
        return true;
    }
}