function prikaziDostupne(){
    $('#tabela-kuponi-obrisani').css('display', 'none');
    $('#tabela-kuponi').css('display', 'block');
    $('#tabela-kuponi-obrisani_wrapper').css('display', 'none');
    $('#tabela-kuponi_wrapper').css('display', 'block');

    $('#kuponi-title').html('Dostupni kuponi');
}

function prikaziNedostupne(){
    $('#tabela-kuponi-obrisani').css('display', 'block');
    $('#tabela-kuponi').css('display', 'none');
    $('#tabela-kuponi-obrisani_wrapper').css('display', 'block');
    $('#tabela-kuponi_wrapper').css('display', 'none');

    $('#kuponi-title').html('Obrisani kuponi');
}