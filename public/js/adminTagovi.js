function prikaziDostupne(){
    $('#tabela-tagovi-obrisani').css('display', 'none');
    $('#tabela-tagovi-aktivni').css('display', 'block');
    $('#tabela-tagovi-obrisani_wrapper').css('display', 'none');
    $('#tabela-tagovi-aktivni_wrapper').css('display', 'block');

    $('#tagovi-title').html('Dostupni tagovi');
}

function prikaziNedostupne(){
    $('#tabela-tagovi-obrisani').css('display', 'block');
    $('#tabela-tagovi-aktivni').css('display', 'none');
    $('#tabela-tagovi-obrisani_wrapper').css('display', 'block');
    $('#tabela-tagovi-aktivni_wrapper').css('display', 'none');

    $('#tagovi-title').html('Obrisani tagovi');
}