function inicijalizujSlike(){
    $('#product-details-slider').slickLightbox();
}

// product Details page item ++/--
$(document).on('click', '.qtplus', function () {
    var total = $('.qttotal').text();
    total++;
    $('.qttotal').text(total);
});
$(document).on('click', '.qtminus', function () {
    var total = $('.qttotal').text();
    total--;
    if (total > 0) {
        $('.qttotal').text(total);
    }
});