function potvrdiPorudzbinu() {
    $usloviError = $('#uslovi-error');
    $usloviError.hide();

    let usloviCB = document.getElementById('usloviKoriscenjaCB');

    if(!usloviCB.checked){
        $usloviError.show();
        return false;
    }

    return true;
}