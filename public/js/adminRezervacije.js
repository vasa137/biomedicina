

function prikaziNaCekanju(){
    $naCekanjuTabela = $('#tabela-rezervacije-na-cekanju');
    $naCekanjuTabelaWrapper = $('#tabela-rezervacije-na-cekanju_wrapper');
    $potvrdjeneTabela = $('#tabela-potvrdjene-rezervacije');
    $potvrdjeneTabelaWrapper = $('#tabela-potvrdjene-rezervacije_wrapper');
    $odbijeneTabela = $('#tabela-odbijene-rezervacije');
    $odbijeneTabelaWrapper = $('#tabela-odbijene-rezervacije_wrapper');
    $rezervacijeTitle = $('#rezervacije-title');

    $naCekanjuTabela.css('display', 'block');
    $naCekanjuTabelaWrapper.css('display', 'block');
    $potvrdjeneTabela.css('display', 'none');
    $potvrdjeneTabelaWrapper.css('display', 'none');
    $odbijeneTabela.css('display', 'none');
    $odbijeneTabelaWrapper.css('display', 'none');

    $rezervacijeTitle.html('Rezervacije na čekanju');
}

function prikaziPotvrdjene(){
    $naCekanjuTabela = $('#tabela-rezervacije-na-cekanju');
    $naCekanjuTabelaWrapper = $('#tabela-rezervacije-na-cekanju_wrapper');
    $potvrdjeneTabela = $('#tabela-potvrdjene-rezervacije');
    $potvrdjeneTabelaWrapper = $('#tabela-potvrdjene-rezervacije_wrapper');
    $odbijeneTabela = $('#tabela-odbijene-rezervacije');
    $odbijeneTabelaWrapper = $('#tabela-odbijene-rezervacije_wrapper');
    $rezervacijeTitle = $('#rezervacije-title');

    $naCekanjuTabela.css('display', 'none');
    $naCekanjuTabelaWrapper.css('display', 'none');
    $potvrdjeneTabela.css('display', 'block');
    $potvrdjeneTabelaWrapper.css('display', 'block');
    $odbijeneTabela.css('display', 'none');
    $odbijeneTabelaWrapper.css('display', 'none');

    $rezervacijeTitle.html('Potvrđene rezervacije');
}

function prikaziOdbijene(){
    $naCekanjuTabela = $('#tabela-rezervacije-na-cekanju');
    $naCekanjuTabelaWrapper = $('#tabela-rezervacije-na-cekanju_wrapper');
    $potvrdjeneTabela = $('#tabela-potvrdjene-rezervacije');
    $potvrdjeneTabelaWrapper = $('#tabela-potvrdjene-rezervacije_wrapper');
    $odbijeneTabela = $('#tabela-odbijene-rezervacije');
    $odbijeneTabelaWrapper = $('#tabela-odbijene-rezervacije_wrapper');
    $rezervacijeTitle = $('#rezervacije-title');

    $naCekanjuTabela.css('display', 'none');
    $naCekanjuTabelaWrapper.css('display', 'none');
    $potvrdjeneTabela.css('display', 'none');
    $potvrdjeneTabelaWrapper.css('display', 'none');
    $odbijeneTabela.css('display', 'block');
    $odbijeneTabelaWrapper.css('display', 'block');

    $rezervacijeTitle.html('Odbijene rezervacije');
}