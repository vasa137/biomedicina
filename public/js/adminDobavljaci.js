function prikaziDostupne(){
    $('#tabela-dobavljaci-obrisani').css('display', 'none');
    $('#tabela-dobavljaci-aktivni').css('display', 'block');
    $('#tabela-dobavljaci-obrisani_wrapper').css('display', 'none');
    $('#tabela-dobavljaci-aktivni_wrapper').css('display', 'block');

    $('#dobavljaci-title').html('Dostupni dobavljači');
}

function prikaziNedostupne(){
    $('#tabela-dobavljaci-obrisani').css('display', 'block');
    $('#tabela-dobavljaci-aktivni').css('display', 'none');
    $('#tabela-dobavljaci-obrisani_wrapper').css('display', 'block');
    $('#tabela-dobavljaci-aktivni_wrapper').css('display', 'none');

    $('#dobavljaci-title').html('Obrisani dobavljači');
}