function validirajVreme(){
    let datum = document.getElementById('datum').value;
    let vreme = parseInt(document.getElementById('vreme_pocetka').value);

    let vremeString = parseInt(vreme/60) + ':'  + vreme%60;

    let izabranoVreme = new Date(datum + ' ' + vremeString);

    let today = new Date();

    if(izabranoVreme.getTime() < today.getTime()){
        document.getElementById('errorMessage').innerHTML = 'Ne možete izabrati vreme u prošlosti';
        return false;
    } else{
        return true;
    }
}

function proveriRezervacije(){
    let datum = document.getElementById('datum').value;
    let vreme = parseInt(document.getElementById('vreme_pocetka').value);

    if(datum == ''){
        return;
    }

    let vremeString = parseInt(vreme/60) + ':'  + vreme%60;

    let izabranoVreme = new Date(datum + ' ' + vremeString);

    document.getElementById('errorMessage').innerHTML = '';
    document.getElementById('successMessage').innerHTML = '';

    let today = new Date();

    if(izabranoVreme.getTime() < today.getTime()){
        document.getElementById('errorMessage').innerHTML = 'Ne možete izabrati vreme u prošlosti';
    } else{
        $.get( "/proveriRezervacije?datum=" + datum + "&vreme_pocetka="  + vreme, function( data ) {
            if(parseInt(data) == 0){
                document.getElementById('errorMessage').innerHTML = 'Postoji velika šansa da će rezervacija u odabranom terminu biti odbijena zbog toga što u to vreme već postoji prihvaćena rezervacija.';
            } else{
                document.getElementById('successMessage').innerHTML = 'Postoji velika šansa da će vaša rezervacija biti prihvaćena, jer u odabranom terminu ne postoji niti jedna prihvaćena rezervacija.';
            }
        })
    }


}