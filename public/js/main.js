$(function ($) {
    "use strict";

    //veno-box js
    $('.venobox').venobox();

    var $window = $(window);
    var html_body = $('html, body')

    //for scroll bottom to top js here
    if ($('.totop').length) {
        var scrollTrigger = 150, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('.totop').addClass('show');
                } else {
                    $('.totop').removeClass('show');
                }
            };

        backToTop();
        $window.on('scroll', function () {
            backToTop();
            if ($window.scrollTop()) {
                $("#mainHeader").addClass('stiky');
            } else {
                $("#mainHeader").removeClass('stiky');
            }
        });

        $('.totop').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }

    // counter js

    $('.count').counterUp({
        delay: 10,
        time: 2000
    });






    //Site Preloader
    jQuery(window).on('load', function () {
        var $sitePreloaderSelector = $('.site-preloader');
        $sitePreloaderSelector.fadeOut(500);


    });





    $(document).ready(function () {
/*
        // product Details page item ++/--
        $(document).on('click', '.qtplus', function () {
            var total = $('.qttotal').text();
            total++;
            $('.qttotal').text(total);
        });
        $(document).on('click', '.qtminus', function () {
            var total = $('.qttotal').text();
            total--;
            if (total > 0) {
                $('.qttotal').text(total);
            }
        });
        */
        // :: 2.0 Slick Active Code
        var slikkfor = $('.slider-for');
       if(slikkfor.length > 0){
        slikkfor.slick({
            slidesToShow: 1,
            speed: 500,
            arrows: false,
            fade: true,
            asNavFor: '.product-deails-thumb'
        });
       }
       var productDetailsThumb = $('.product-deails-thumb');

       if(productDetailsThumb.length > 0){

            productDetailsThumb.slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                speed: 500,
                asNavFor: '.slider-for',
                dots: false,
                focusOnSelect: true,
                slide: 'li',
                autoplay: true,
                arrows: false
            });

       }

      

    });


}(jQuery));