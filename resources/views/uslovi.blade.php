@extends('layout')

@section('title')
Uslovi kupovine - 
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Uslovi kupovine
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Naslovna</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="/uslovi-kupovine">Uslovi kupovine</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->
 <!-- FAQ Section Start-->
 <section class="faqSection">
	<div class="container">
		<p>
			
		U skladu sa odredbama Zakona o zaštiti potrošača ( “Sl. Glasnik RS”, br 62/2014 I 6/2016), a u cilju obaveštavanja kupaca o njihovim pravima, direktor preduzeće Essence of Beauty objavljuje OBAVEŠTENJE O POSTUPANJU PO REKLAMACIJAMA
		<br><br>
		Član 1.
		<br><br>
		Ovo Obaveštenje odnosi se na način I uslove izjavljivanja, kao i postupak rešavanja po izjavljenoj reklamaciji kupaca na robu kupljenu preko sajta (web shopa) Essence of Beauty.
		<br><br>
		1. PRAVO KUPCA NA IZJAVLJIVANJE REKLAMACIJE
		<br><br>
		1.1. Kupac ima pravo da izjavi reklamaciju u slučaju da se, nakon prijema poručene robe, ustanovi da proizvod ima neki tehnološki nedostatak (ne radi pumpica na kremi ili slično), ili skrivenu manu (izmenjen sastav proizvoda), a što kupcu onemogućava redovnu upotrebu proizvoda.
		<br><br>
		2. USLOVI I NAČIN IZJAVLJIVANJA REKLAMACIJE
		<br><br>
		2.1. Zahtev za reklamaciju podnosi se na posebnom obrascu , u roku od 8 dana od dana prijema poručene robe.
		<br><br>
		2.2. Popunjen i potpisan Zahtev za reklamaciju ,kopija računa, roba koja je predmet reklamacije , sva primljenu dokumentacija i drugi dokazi o kupovini dostavljaju se posredstvom pošte ili druge kurirske službe, ili lično na adresu preduzeća Essence of Beauty, Bulevar Zorana Đinđića 6, 11000 Beograd
		<br><br>
		2.3. Ukoliko se reklamirana roba vraća prodavcu putem pošte ili druge kurirske službe, kupac je u obavezi da pre slanja proizvod adekvatno upakuje i čitko adresira paket. Prodavac ne preuzima odgovornost za gubitak, lom ili oštećenja pošiljke u transportu, a koja su posledica neadekvatnog pakovanja ili adresiranja pošiljke.
		Zaktev za reklamaciju se podnosi na posebnom
		<br><br>
		3. POSTUPAK I ROK ZA ODLUČIVANJE PO IZJAVLENOJ REKLAMACIJI
		<br><br>
		3.1. Prodavac je dužan da izda pisanu potvrdu ili elektronskim putem potvrdi prijem reklamacije i broj pod kojim je reklamacija zavedena, kao i da o izjavljenim reklamacijama na isporučeni proizvod odluči na način I u roku definisanim ovim Pravilnikom I Zakonom o zaštiti potrošača.
		<br><br>
		3.2. Prodavac je dužan da vodi evidenciju izjavljenih reklamacija u elektronskoj formi.
		<br><br>
		3.3. Po prijemu Zahteva za reklamaciju, Prodavac će ispitati navode i na adekvatan način pristupiti testiranju proizvoda, te odlučiti o njenoj osnovanosti i bez odlaganja, a najkasnije u roku od osam dana od dana prijema reklamacije, pisanim ili elektronskim putem odgovoriti Kupcu na izjavljenu reklamaciju. Odgovor prodavca na reklamaciju potrošača mora da sadrži odluku da li prihvata reklamaciju, izjašnjenje o zahtevu potrošača i konkretan predlog i rok za rešavanje reklamacije. Rok ne može da bude duži od 15 dana, od dana podnošenja reklamacije.
		<br><br>
		4. ODLUKE PRODAVCA PO IZJAVLJENIM REKLAMACIJAMA
		<br><br>
		4.1. U slučaju da se prilikom ispitivanja osnovanosti reklamacije utvrdi da na proizvodu nema nedostataka, ili se utvrdi da postoje: – neznatni nedostaci na proizvodima, koji ne utiču na ispravnu upotrebu proizvoda;ili neznatana oštećenja na amabalaži. nastala u transportu tokom isporuke proizvoda – odstupanje boje proizvoda od boje na sajtu ili da je nedostatak na proizvodu nastao nakon preuzimanja proizvoda , kao posledica postupka kupca ili trećeg lica kome je proizvod poslat -izjavljena reklamacija SE ODBIJA, a reklamirani proizvod se vraća uz Odluku o reklamaciji.
		<br><br>
		4.2. U slučaju da je reklamacija opravdana, Prodavac donosi Odluku kojom se reklamacija usvaja I rešava na jedan od načina: – Ako je moguće, otklanja se nedostatak na proizvodu – Ako nije moguće otkloniti nedostatak na proizvodu, Prodavac će zameniti proizvod za isti proizvod bez nedostatka, ili drugi proizvod po zboru Kupca iste vrednosti – Ukoliko Prodavac nema više isti proizvod na lageru, Kupcu će biti ponuđeno da sačeka sledeću isporuku ili da zatraži povraćaj novca
		<br><br>
		Ovo Obaveštenje istaknuto je na website essenceofbeauty.rs dana 5.5.2019. čime se Prodavac oslobađa obaveštavanja kupaca na drugi način.
		<br><br>
		U Beogradu, 5.5.2019. Essence of Beauty
		<br><br>

		Cena dostave je 280 dinara. * Za porudžbine preko 4000 dostava je besplatna.
		<br><br>
		*cena može biti promenjena po redovnom cenovniku kurirske službe City Express.

		</p>
	</div>
</section>
<!-- FAQ Section Start-->



@stop