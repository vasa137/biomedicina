@extends('layout')

@section('title')
Edukacija - 
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Edukacija
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Naslovna</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="/edukacija">Edukacija</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->

<!-- Main Blog Sectin Srart -->
<section id="mainBlog" class="mainBlog">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/bb-glow">
						<img class="img-fluid" src="img/edukacija/bb-glow.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/bb-glow" title="BB Glow tretman edukacija">BB Glow</a></h3>
						<h3 class="post-date">CENA: od 22,500 rsd</h3>
						<div>
							<p>Želite i Vi da svojim klijentima pružite tretman za kojim su poludele čak i holivudske zvezde no ne znate kome od edukatora da poverite svoje vreme i novac kako bi bili sigurni da ćete uspešno ovladati ovom tehnikom i među prvima postati sertifikovani visoko profesionalni BB Glow terapeut?</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/bb-glow">Detaljnije<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/maderoterapija">
						<img class="img-fluid" src="img/edukacija/maderoterapija.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/maderoterapija" title="BB Glow tretman edukacija">Maderoterapija</a></h3>
						<h3 class="post-date">CENA: od 25,000 rsd</h3>
						<div>
							<p>Čuli ste da je anticelulit masaža oklagijom najdelotvorniji i najprirodniji način za eliminaciju celulita, a takodje i najtraženija usluga u kozmetičkim salonima? Jako želite da se bavite tim veoma profitabilnim zanimanjem ali nemate od koga da naučite? Kontaktirajte nas što pre jer danas u našem edukativnom centru počinje prijava na obuku za terapeuta maderoterapije.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/maderoterapija">Detaljnije<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/maderoterapija-lica">
						<img class="img-fluid" src="img/edukacija/maderoterapija-lica.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/maderoterapija-lica" title="BB Glow tretman edukacija">Maderoterapija Lica</a></h3>
						<h3 class="post-date">CENA: od 12,000 rsd</h3>
						<div>
							<p>Osim maderoterapije tela koja je postala apsolutno najpopularnija tehnika oslobađanja od celulita,  maderoterapija lica je takođe sve traženiji tretman u salonima lepote.Oni koji su probali  facijalnu maderoterapiju uverili su se da ona dovodi do poboljšanja tonusa kože lica i daje efekat podmlađivanja jer podstiče proizvodnju elastina i kolagena.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/maderoterapija-lica">Detaljnije<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/anticelulit-masaza">
						<img class="img-fluid" src="img/edukacija/anticelulit-masaza.jpg" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						<h3 class="title"><a href="/edukacija/anticelulit-masaza" title="BB Glow tretman edukacija">Anticelulit Masaža</a></h3>
						<h3 class="post-date">CENA: 25,000 rsd</h3>
						<div>
							<p>Kod žena je utemeljeno mišljenje da je ručna anticelulit masaža najefikasniji način uklanjanja celulita. Cilj naše edukacije je da vas naučimo kako da pravilno sprovodite anticelulit masažu kako bi se telo klijenta potpuno oslobodilo tzv narandžine kore.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/anticelulit-masaza">Detaljnije<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-6 col-lg-4">
				<div class="blog-box">
					<div class="topImg">
						<a href="/edukacija/relaks-masaza">
						<img class="img-fluid" src="img/edukacija/relaks-masaza.png" alt="">
						</a>
					</div>
					<div class="blog-item-description">
						
						<h3 class="title"><a href="/edukacija/relaks-masaza" title="BB Glow tretman edukacija">Relaks Masaža</a></h3>
						<h3 class="post-date">CENA: 27,000 rsd</h3>
						<div>
							<p>Ako ste do sada pokazivali sklonosti prema masaži i zaista želite time da se bavite onda je ovo jedinstvena prilika da ovladate relaks masažom. Zbog svakodnevnog  sveprisutnog stresa sve veći broj ljudi oseća se iscrpljeno, napeto i ima veliku potrebu za relaksacijom. Mi ćemo vas naučiti kako da klijentima prenesete svoju pozitivnu energiju i na taj način im pomognete da poboljšaju svoje psiho-fizičko stanje.</p>
							<div class="read-more-link-wrapper">
								<a class="read-more-link" href="/edukacija/relaks-masaza">Detaljnije<i class="fas fa-angle-right"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Main Blog Sectin End -->
@stop