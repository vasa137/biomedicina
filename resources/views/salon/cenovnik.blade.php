@extends('layout')

@section('title')
Cenovnik - Salon Lepote - 
@stop

@section('scriptsTop')
<link rel="stylesheet" type="text/css" href="{{asset('css/cenovnik.css')}}">
@stop

@section('scriptsBottom')
<script src="{{asset('js/cenovnik.js')}}"></script>
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Cenovnik
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/salon">Salon Lepote</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="" href="/salon/cenovnik">Cenovnik</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				

				<button class="collapsible">TRETMANI LICA</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Higijenski tretman lica</h5>  <h5 style="float: right;">3300 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Tretman lica za tinejdžere</h5>  <h5 style="float: right;">2700 rsd <br></h5>
							<br>
							<h5 style="float: left;">Hydralift tretman lica</h5>  <h5 style="float: right;">3900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Antiage tretman lica</h5>  <h5 style="float: right;">3900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Biostimulativni tretman lica</h5>  <h5 style="float: right;">4500 rsd <br></h5>
							<br>
							<h5 style="float: left;">Tretman lica voćnim kiselinama</h5>  <h5 style="float: right;">3000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Ultrazvučno čišćenje lica</h5>  <h5 style="float: right;">1900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Ultrazvučno čišćenje uz tretman</h5>  <h5 style="float: right;">900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Mezoterapija lica</h5>  <h5 style="float: right;">3900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Mezoterapija lica i vrata</h5>  <h5 style="float: right;">4900 rsd <br></h5> 
						</div>

				

						<div class="col-6">
							<h5 style="float: left;">Mezoterap. lica, vrata i dekoltea</h5>  <h5 style="float: right;">5900 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Dermapen tretman lica</h5>  <h5 style="float: right;">4900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Dermapen lica i vrata</h5>  <h5 style="float: right;">5900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Dermapen lica, vrata i dekoltea</h5>  <h5 style="float: right;">6900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Lifting masaža lica</h5>  <h5 style="float: right;">800 rsd <br></h5>
							<br>
							<h5 style="float: left;">Maderoterapija lica</h5>  <h5 style="float: right;">1300 rsd <br></h5>
							<br>
							<h5 style="float: left;">BB Glow tretman</h5>  <h5 style="float: right;">11900 rsd <br></h5>
							<br>
							<h5 style="float: left;">BB Glow 2. tretman do 14 dana</h5>  <h5 style="float: right;">5900 rsd <br></h5>
							<br>
							<h5 style="float: left;">BB Glow regije oka</h5>  <h5 style="float: right;">3900 rsd <br></h5>
							<br>
							<h5 style="float: left;">BB Glow regije oka do 14 dana </h5>  <h5 style="float: right;">1900 rsd <br></h5>
						</div>
					</div>
				</div>				
				<br>
				<button class="collapsible">DEPILACIJA ZA DAME</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Depilacija (ruke, noge, brazilka)</h5>  <h5 style="float: right;">1950 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Depilacija polovine ruku</h5>  <h5 style="float: right;">300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija ruku</h5>  <h5 style="float: right;">550 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija polovine nogu</h5>  <h5 style="float: right;">550 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Depilacija celih nogu</h5>  <h5 style="float: right;">950 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija prepona</h5>  <h5 style="float: right;">300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija tanga zone</h5>  <h5 style="float: right;">450 rsd <br></h5>
							<br>
							<h5 style="float: left;">Brazilska depilacija</h5>  <h5 style="float: right;">800 rsd <br></h5>  
						</div>
						<div class="col-6">
							<h5 style="float: left;">Depilacija leđa</h5>  <h5 style="float: right;">450 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Depilacija stomaka</h5>  <h5 style="float: right;">450 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija linije stomaka</h5>  <h5 style="float: right;">300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija pazuha</h5>  <h5 style="float: right;">300 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Depilacija obrva</h5>  <h5 style="float: right;">250 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija nausnica</h5>  <h5 style="float: right;">250 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija brade</h5>  <h5 style="float: right;">200 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija celog lica</h5>  <h5 style="float: right;">600 rsd <br></h5> 
						</div>
					</div>
				</div>

				<br>
				<button class="collapsible">DEPILACIJA ZA GOSPODU</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Depilacija ramena</h5>  <h5 style="float: right;">400 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Depilacija leđa</h5>  <h5 style="float: right;">900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija rukU</h5>  <h5 style="float: right;">800 rsd <br></h5>   
						</div>
						<div class="col-6">
							<h5 style="float: left;">Depilacija grudi</h5>  <h5 style="float: right;">700 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Depilacija stomaka</h5>  <h5 style="float: right;">700 rsd <br></h5>
							<br>
							<h5 style="float: left;">Depilacija celih nogu</h5>  <h5 style="float: right;">1300 rsd <br></h5>
						</div>
					</div>
				</div>
				<br>

				<button class="collapsible">PEDIKIR USLUGE</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Estetski pedikir</h5>  <h5 style="float: right;">1300 rsd <br></h5> 
							<br>
							<h5 style="float: left;">SPA pedikir</h5>  <h5 style="float: right;">1600 rsd <br></h5>
							<br>
							<h5 style="float: left;">Parafinsko pakovanje nogu</h5>  <h5 style="float: right;">700 rsd <br></h5>   
						</div>
						<div class="col-6">
							<h5 style="float: left;">Estetski pedikir sa trajnim lakom</h5>  <h5 style="float: right;">1700 rsd <br></h5> 
							<br>
							<h5 style="float: left;">SPA pedikir sa trajnim lakom</h5>  <h5 style="float: right;">2000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Lakiranje noktiju</h5>  <h5 style="float: right;">300 rsd <br></h5>
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">MANIKIR USLUGE</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Klasičan manikir</h5>  <h5 style="float: right;">700 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Klas. manikir sa trajnim lakom</h5>  <h5 style="float: right;">1200 rsd <br></h5>
							<br>
							<h5 style="float: left;">SPA manikir</h5>  <h5 style="float: right;">1100 rsd <br></h5>
							<br>
							<h5 style="float: left;">SPA manikir sa trajnim lakom</h5>  <h5 style="float: right;">1600 rsd <br></h5>
							<br>
							<h5 style="float: left;">Japanski manikir</h5>  <h5 style="float: right;">1200 rsd <br></h5> 
						</div>
						<div class="col-6">
							<h5 style="float: left;">Parafinsko pakovanje ruku</h5>  <h5 style="float: right;">500 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Lakiranje noktiju lakom</h5>  <h5 style="float: right;">300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Ojačanje noktiju gelom</h5>  <h5 style="float: right;">1300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Uklanjanje trajnog laka</h5>  <h5 style="float: right;">300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Uklanjanje nadogradnje ili gela</h5>  <h5 style="float: right;">500 rsd <br></h5> 
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">MASAŽE I TRETMANI TELA</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Anticelulit masaža 30min</h5>  <h5 style="float: right;">1100 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Anticelulit masaža 45min</h5>  <h5 style="float: right;">1300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Maderoterapija 30min</h5>  <h5 style="float: right;">1300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Maderoterapija 45min</h5>  <h5 style="float: right;">1600 rsd <br></h5>
							<br>
							<h5 style="float: left;">Terapeutska masaža 30min</h5>  <h5 style="float: right;">1600 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Terapeutska masaža 45min</h5>  <h5 style="float: right;">2000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Sportska masaža 45min</h5>  <h5 style="float: right;">1700 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Sportska masaža 60min</h5>  <h5 style="float: right;">2200 rsd <br></h5>
							<br>
							<h5 style="float: left;">Limfna drenaža 30min</h5>  <h5 style="float: right;">1500 rsd <br></h5>
							<br>
							<h5 style="float: left;">Limfna drenaža 45min</h5>  <h5 style="float: right;">1900 rsd <br></h5>  
							<br>
							<h5 style="float: left;">Relaks masaža 45min</h5>  <h5 style="float: right;">1700 rsd <br></h5> 
						</div>
						<div class="col-6">
							<h5 style="float: left;">Relaks masaža 60min</h5>  <h5 style="float: right;">2100 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Coconut magic tretman 90min</h5>  <h5 style="float: right;">3100 rsd <br></h5>
							<br>
							<h5 style="float: left;">Čoko SPA tretman 60min</h5>  <h5 style="float: right;">3300 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Marokanski SPA tretman 90min</h5>  <h5 style="float: right;">3500 rsd <br></h5>
							<br>
							<h5 style="float: left;">Harmony&Balance tret. 90min</h5>  <h5 style="float: right;">3700 rsd <br></h5>
							<br>
							<h5 style="float: left;">Elektrostimulacija mišića</h5>  <h5 style="float: right;">1000 rsd <br></h5> 
							<br>
							<h5 style="float: left;">IR detox tretman 40 minuta</h5>  <h5 style="float: right;">1100 rsd <br></h5>
							<br>
							<h5 style="float: left;">Gipsane bermude</h5>  <h5 style="float: right;">3900 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Ultimate slim  tretman</h5>  <h5 style="float: right;">3500 rsd <br></h5>
							<br>
							<h5 style="float: left;">Vacuslim 48  tretman</h5>  <h5 style="float: right;">2700 rsd <br></h5>
							<br>
							<h5 style="float: left;">Piling tela</h5>  <h5 style="float: right;">1400 rsd <br></h5>
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">PAKETI MASAŽA I TRETMANA TELA</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">10 anticelulit masaža od 30min</h5>  <h5 style="float: right;">9000 rsd <br></h5> 
							<br>
							<h5 style="float: left;">10 anticelulit masaža od 45min</h5>  <h5 style="float: right;">10000 rsd <br></h5>
							<br>
							<h5 style="float: left;">10 maderoterapija od 30min</h5>  <h5 style="float: right;">11000 rsd <br></h5>   
						</div>
						<div class="col-6">
							<h5 style="float: left;">10 maderoterapija od 45min</h5>  <h5 style="float: right;">14000 rsd <br></h5> 
							<br>
							<h5 style="float: left;">10 elektrostimulacija</h5>  <h5 style="float: right;">8000 rsd <br></h5>
							<br>
							<h5 style="float: left;">10 IR detox tretmana</h5>  <h5 style="float: right;">9000 rsd <br></h5>
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">EPILACIJA ZA DAME</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Epilacija pola nogu</h5>  <h5 style="float: right;">5900 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Epil. nogu sa plitkim preponama</h5>  <h5 style="float: right;">9900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija prepona</h5>  <h5 style="float: right;">1600 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija tanga zone</h5>  <h5 style="float: right;">2900 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija tzv brazilska</h5>  <h5 style="float: right;">4400 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija pola ruku</h5>  <h5 style="float: right;">2800 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija celih ruku</h5>  <h5 style="float: right;">5000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija leđa</h5>  <h5 style="float: right;">4000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija stomaka</h5>  <h5 style="float: right;">3500 rsd <br></h5>  
						</div>
						<div class="col-6">
							<h5 style="float: left;">Epilacija linije stomaka</h5>  <h5 style="float: right;">1500 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Epilacija grudi</h5>  <h5 style="float: right;">1400 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija pazuha</h5>  <h5 style="float: right;">3300 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija vrata</h5>  <h5 style="float: right;">1600 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija nausnica</h5>  <h5 style="float: right;">1450 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija brade</h5>  <h5 style="float: right;">1650 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija zulufa</h5>  <h5 style="float: right;">1100 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija obraza</h5>  <h5 style="float: right;">2200 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija celog lica</h5>  <h5 style="float: right;">3950 rsd <br></h5> 
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">EPILACIJA ZA GOSPODU</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Epilacija pola nogu</h5>  <h5 style="float: right;">7800 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Epilacije celih nogu</h5>  <h5 style="float: right;">12000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija pola ruku</h5>  <h5 style="float: right;">3500 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Epilacija celih ruku</h5>  <h5 style="float: right;">6500 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija polovine leđa</h5>  <h5 style="float: right;">4200 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija celih leđa</h5>  <h5 style="float: right;">7200 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija ramena</h5>  <h5 style="float: right;">2500 rsd <br></h5>  
							<br>
							<h5 style="float: left;">Epilacija stomaka</h5>  <h5 style="float: right;">4000 rsd <br></h5> 
						</div>
						<div class="col-6">
							<h5 style="float: left;">Epilacija grudi</h5>  <h5 style="float: right;">3800 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija pazuha</h5>  <h5 style="float: right;">3600 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Epilacija vrata</h5>  <h5 style="float: right;">3100 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija nausnica</h5>  <h5 style="float: right;">2100 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija brade</h5>  <h5 style="float: right;">3000 rsd <br></h5>
							<br>
							<h5 style="float: left;">Epilacija zulufa</h5>  <h5 style="float: right;">1800 rsd <br></h5>  
							<br>
							<h5 style="float: left;">Epilacija jagodica</h5>  <h5 style="float: right;">1800 rsd <br></h5> 
						</div>
					</div>
				</div>

				<br>

				<button class="collapsible">OSTALE USLUGE</button>
				<div class="content">
					<div class="row">
						<div class="col-6">
							<h5 style="float: left;">Farbanje obrva</h5>  <h5 style="float: right;">300 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Farbanje trepavica</h5>  <h5 style="float: right;">300 rsd <br></h5>
						</div>
						<div class="col-6">
							<h5 style="float: left;">Korekcija obrva pincetom</h5>  <h5 style="float: right;">300 rsd <br></h5> 
							<br>
							<h5 style="float: left;">Lash lift</h5>  <h5 style="float: right;">2500 rsd <br></h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<br>
	<br>
	<br>
	
	<div class="container" style="text-align: center;">
		<h5>
			*Za uplatu paketa od 3 masaže unapred odobravamo 15% popusta<br>
			**Pakete uplaćenih usluga potrebno je iskoristiti u roku od 60 dana<br>
			***Kod duo masaže 2.osoba plaća 60% cene navedene u Cenovniku<br>
		</h5>
	</div>
</section>
<!-- Video Section End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Željene tretmane možete zakazati na broj: <strong><a href="tel:069/455-90-99">069/455-90-99</a></strong> 
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



