@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Rezervacija - #{{$rezervacija->id}} {{$rezervacija->usluga->naziv}}, {{$rezervacija->klijent}}
    @else
        Nova rezervacija
    @endif
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <a class="breadcrumb-item" href="/admin/rezervacije">Rezervacije</a>
    <span class="breadcrumb-item active">@if($izmena)#{{$rezervacija->id}} {{$rezervacija->usluga->naziv}}, {{$rezervacija->klijent}} @else Nova rezervacija @endif</span>
@stop

@section('heder-h1')
    @if($izmena)#{{$rezervacija->id}} {{$rezervacija->usluga->naziv}}, {{$rezervacija->klijent}} @else Nova rezervacija @endif
@stop


@section('scriptsTop')
    <link href="{{asset('css/adminRezervacija.css')}}" rel="stylesheet"/>
    <script src="{{asset('js/adminRezervacija.js')}}"></script>

@endsection

@section('scriptsBottom')

@endsection

@section('main')
    <div class="row gutters-tiny">

        @if(!$izmena or $rezervacija->admin_kreirao)
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-rezervacija-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->
        @endif
    @if($izmena and $rezervacija->admin_kreirao)
            @if(!$rezervacija->sakriven)
            <!-- Delete Product -->

                <div class="col-md-3 col-xl-3">
                    <form id="forma-obrisi-rezervaciju" method="POST" action="/admin/obrisiRezervaciju/{{$rezervacija->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-rezervaciju').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-trash fa-2x text-danger"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-danger">
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši rezervaciju</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>



            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-rezervaciju" method="POST" action="/admin/restaurirajRezervaciju/{{$rezervacija->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-rezervaciju').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj rezervaciju</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
            @endif
        @endif

    </div>

    @if($izmena and !$rezervacija->admin_kreirao)
        <h2 class="content-heading">Promena statusa rezervacije</h2>
        <div class="row row-deck gutters-tiny">
            <!-- END Progress -->
            <div  id="forma-akcije" >

                <a class="text-success btn btn-sm btn-secondary button-akcija" data-toggle="tooltip" title="Potvrdi" href="javascript:$('#forma-rezervacija-submit-button').click()">
                    <i class="fa fa-check fa-3x"></i>
                </a>




                <form method="POST" style="display:inline;">
                {{csrf_field()}}
                @if($rezervacija->status != 'na_cekanju')
                    <button type="submit" class="text-warning btn btn-sm btn-secondary button-akcija" data-toggle="tooltip" title="Vrati na čekanje" formaction="/admin/statusRezervacije/{{$rezervacija->id}}/na_cekanju">
                        <i class="fa fa-flag fa-3x"></i>
                    </button>
                @endif

                @if($rezervacija->status != 'odbijena')
                    <button type="submit" class="text-danger btn btn-sm btn-secondary button-akcija"  data-toggle="tooltip" title="Odbij" formaction="/admin/statusRezervacije/{{$rezervacija->id}}/odbijena">
                        <i class="fa fa-times fa-3x"></i>
                    </button>
                @endif
                </form>
            </div>
        </div>
        <br/>
    @endif
    <!-- END Overview -->
    @if($izmena and !$rezervacija->admin_kreirao)
    <form id="forma-rezervacija" method="POST" action="/admin/potvrdiRezervaciju/{{$rezervacija->id}}" onsubmit="return validirajVreme()">
    @else
    <form id="forma-rezervacija" method="POST" @if($izmena) action="/admin/sacuvajRezervaciju/{{$rezervacija->id}}" @else action="/admin/sacuvajRezervaciju/-1" @endif onsubmit="return validirajVreme()">
    @endif
    {{csrf_field()}}
    <!-- Update Product -->
        <!-- END Billing Address -->
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-7">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Informacije</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Naziv usluge</label>
                            <div class="col-12 input-group">

                                @if(!$izmena or $rezervacija->admin_kreirao)
                                    <select id="id_usluga"  class="form-control" name="id_usluga" @if($izmena and !$rezervacija->admin_kreirao) readonly @endif>
                                        @foreach($usluge as $usluga)
                                            <option value="{{$usluga->id}}" @if($izmena and $rezervacija->usluga->id == $usluga->id) selected @endif>{{$usluga->naziv}}</option>
                                        @endforeach
                                    </select>

                                @else
                                    <input id="id_usluga"  maxlength="254" type="text" class="form-control" name="id_usluga" value="{{$rezervacija->usluga->naziv}}" readonly>
                                @endif

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" >Klijent</label>
                            <div class="col-12 input-group">
                                <input id="klijent"  maxlength="254" type="text" class="form-control" name="klijent" @if($izmena) value="{{$rezervacija->klijent}}" @endif @if($izmena and !$rezervacija->admin_kreirao) readonly @endif>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" >Telefon</label>
                            <div class="col-12 input-group">
                                <input id="telefon"  maxlength="19" type="text" class="form-control" name="telefon" @if($izmena) value="{{$rezervacija->telefon}}" @endif @if($izmena and !$rezervacija->admin_kreirao) readonly @endif>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" >Email</label>
                            <div class="col-12 input-group">
                                <input id="email"  maxlength="254" type="email" class="form-control" name="email" @if($izmena) value="{{$rezervacija->email}}" @endif @if($izmena and !$rezervacija->admin_kreirao) readonly @endif>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12" >Napomena</label>
                            <div class="col-12 input-group">
                                <textarea rows="10" cols="50" id="napomena"  maxlength="999" type="text" class="form-control" name="napomena" @if($izmena and !$rezervacija->admin_kreirao) readonly @endif>@if($izmena){{$rezervacija->napomena}}@endif</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Basic Info -->

            <!-- More Options -->
            <div class="col-md-5">
                <!-- Status -->
                <div class="block block-rounded block-themed">


                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-gd-primary">
                            <h3 class="block-title">Vreme rezervacije</h3>
                        </div>
                        <div class="block-content block-content-full row">
                            <div class="col-sm-12">
                                <div class="form-group row">
                                    <label class="col-12" >Datum</label>
                                    <div class="col-12 input-group">
                                        <input id="datum" type="date" class="form-control" name="datum" @if($izmena) value="{{$rezervacija->vreme_pocetka->format('Y-m-d')}}" @endif @if($izmena and !$rezervacija->admin_kreirao) readonly @endif>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-4" >Vreme početka</label>
                                    <div class="col-8 input-group">
                                        <input min="8" max="21" id="vreme_pocetka_h" type="number" class="form-control" name="vreme_pocetka_h" @if($izmena) value="{{$rezervacija->vreme_pocetka->format('H')}}" @endif @if($izmena and !$rezervacija->admin_kreirao) readonly @endif required>
                                        &emsp;:&emsp;
                                        <input min="0" max="59" id="vreme_pocetka_m" type="number" class="form-control" name="vreme_pocetka_m" @if($izmena) value="{{$rezervacija->vreme_pocetka->format('i')}}" @endif @if($izmena and !$rezervacija->admin_kreirao) readonly @endif required>
                                    </div>
                                    <label class="col-4" >Vreme kraja</label>
                                    <div class="col-8 input-group">
                                        <input min="8" max="21" id="vreme_kraja_h" type="number" class="form-control" name="vreme_kraja_h" @if($izmena and $rezervacija->vreme_kraja != null) value="{{$rezervacija->vreme_kraja->format('H')}}" @endif required>
                                        &emsp;:&emsp;
                                        <input min="0" max="59" id="vreme_kraja_m" type="number" class="form-control" name="vreme_kraja_m" @if($izmena and $rezervacija->vreme_kraja != null) value="{{$rezervacija->vreme_kraja->format('i')}}" @endif required>
                                    </div>
                                </div>


                            </div>
                            <p id="errorMessage" style="color:red;"></p>
                        </div>
                    </div>
                    @if($izmena)
                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-gd-primary">
                            <h3 class="block-title">Status rezervacije</h3>
                        </div>

                        <!-- Packaging -->
                        <div class="col-md-3 col-xl-3">
                            <div class="block block-rounded">
                                <div class="block-content block-content-full">
                                    <div class="py-20 text-center">
                                        @if($rezervacija->status == 'na_cekanju')
                                            <div class="mb-20">
                                                <i class="fa fa-flag  fa-3x text-warning"></i>
                                            </div>
                                            <div class="font-size-sm font-w600 text-uppercase text-warning"> Na čekanju </div>
                                        @elseif($rezervacija->status == 'potvrdjena')
                                            <div class="mb-20">
                                                <i class="fa fa-check fa-3x text-success"></i>
                                            </div>
                                            <div class="font-size-sm font-w600 text-uppercase text-success"> Potvrđena </div>
                                        @elseif($rezervacija->status == 'odbijena')
                                            <div class="mb-20">
                                                <i class="fa fa-times fa-3x text-danger"></i>
                                            </div>
                                            <div class="font-size-sm font-w600 text-uppercase text-danger"> Odbijena </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    @endif
                </div>
                    <!-- END Delete Product -->

            </div>



        </div>
        <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-rezervacija-submit-button" style="display:none"/>
    </form>
@stop