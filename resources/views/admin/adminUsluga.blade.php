@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Usluga - {{$usluga->naziv}}
    @else
        Nova usluga
    @endif
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <a class="breadcrumb-item" href="/admin/usluge">Usluge</a>
    <span class="breadcrumb-item active">@if($izmena){{$usluga->naziv}} @else Nova usluga @endif</span>
@stop

@section('heder-h1')
    @if($izmena){{$usluga->naziv}} @else Nova usluga @endif
@stop


@section('scriptsTop')

@endsection

@section('scriptsBottom')

@endsection

@section('main')
    <div class="row gutters-tiny">
    @if($izmena)

        <!-- In Orders -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket fa-2x text-info"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$usluga->broj_rezervacija}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Rezervacija</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END In Orders -->
    @endif
    <!-- Stock -->
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-usluga-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->

    @if($izmena)
        @if(!$usluga->sakriven)
            <!-- Delete Product -->

                <div class="col-md-3 col-xl-3">
                    <form id="forma-obrisi-uslugu" method="POST" action="/admin/obrisiUslugu/{{$usluga->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-uslugu').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-trash fa-2x text-danger"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-danger">
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši uslugu</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-uslugu" method="POST" action="/admin/restaurirajUslugu/{{$usluga->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-uslugu').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj uslugu</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
        @endif
    @endif
    <!-- END Delete Product -->
    </div>
    <!-- END Overview -->
    <form id="forma-usluga" method="POST" @if($izmena) action="/admin/sacuvajUslugu/{{$usluga->id}}" @else action="/admin/sacuvajUslugu/-1" @endif>
    {{csrf_field()}}
    <!-- Update Product -->
        <h2 class="content-heading">Informacije o usluzi</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-7">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Informacije</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Naziv</label>
                            <div class="col-12 input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                                </div>
                                <input id="naziv"  maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$usluga->naziv}}" @endif required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" >Opis</label>
                            <div class="col-lg-12">
                                <div class="input-group">
                                    <input type="text" class="form-control"  name="opis" @if($izmena) value="{{$usluga->opis}}" @endif>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-12" >Iznos</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="fa fa-fw fa-money"></i>
                                </span>
                                    </div>
                                    <input type="number" class="form-control" name="iznos" @if($izmena) value="{{number_format($usluga->iznos, 2, '.', '')}}" @else value="0" @endif required>
                                    <div class="input-group-append">
                                        <span class="input-group-text">rsd</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- END Basic Info -->

            <!-- More Options -->
            <div class="col-md-5">
                <!-- Status -->
                <div class="block block-rounded block-themed">


                    <div class="block block-rounded block-themed">
                        <div class="block-header bg-gd-primary">
                            <h3 class="block-title">Grupa usluga</h3>
                        </div>
                        <div class="block-content block-content-full row">
                            <div class="col-sm-12">
                                @foreach($grupeUsluga as $grupaUsluga)
                                    <label class="css-control css-control-primary css-radio">
                                        <input type="radio" class="css-control-input" name="grupaUsluga" value="{{$grupaUsluga->id}}" @if(!$izmena or ($izmena and $usluga->id_grupa_usluga == $grupaUsluga->id)) checked @endif >
                                        <span class="css-control-indicator"></span> {{$grupaUsluga->naziv}}
                                    </label>
                                    <br/>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-usluga-submit-button" style="display:none"/>
    </form>
@stop