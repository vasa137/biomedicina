@extends('admin.adminLayout')

@section('title')
    Rezervacije
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">Rezervacije</span>
@stop

@section('heder-h1')
    Rezervacije
@stop


@section('heder-h2')
    Trenutno imate<a class="text-primary-light link-effect"> {{count($rezervacijeNaCekanju)}} rezervacija na čekanju</a>.
@stop


@section('scriptsTop')
    <!-- OBAVEZNO rel="stylesheet" -->

    <link rel="stylesheet" href="{{asset('assets/js/plugins/fullcalendar/fullcalendar.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('/js/adminRezervacije.js')}}"></script>
    <script src="{{asset('/js/tabelaRezervacije.js')}}"></script>
    <script src="{{asset('assets/js/plugins/moment/moment.min.js')}}"></script>

    <script src="{{asset('assets/js/plugins/fullcalendar/fullcalendar.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/jquery.fullcalendar.js')}}"></script>

    <script>storeReservations('{!! addslashes($potvrdjeneRezervacije) !!}');</script>
@endsection

<!-- Bootstrap Colorpicker (.js-colorpicker class is initialized in Codebase() -> uiHelperColorpicker()) -->
<!-- For more info and examples you can check out https://github.com/itsjavi/bootstrap-colorpicker/ -->

@section('scriptsBottom')

@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziNaCekanju()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-warning fa-2x text-warning"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($rezervacijeNaCekanju)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">NA ČEKANJU</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziPotvrdjene()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-check fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success" data-toggle="countTo" data-to="{{count($potvrdjeneRezervacije)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-success">POTVRĐENIH</div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="javascript:prikaziOdbijene()">
                <div class="block-content block-content-full block-sticky-options" >
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-times fa-2x text-danger"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($obrisaneOdbijeneRezervacije)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-danger">ODBIJENIH</div>
                    </div>
                </div>
            </a>
        </div>

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/rezervacija/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novu rezervaciju</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>

    <!-- Dynamic Table Full Pagination -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title" id="rezervacije-title">Rezervacije na čekanju</h3>
        </div>
        <div class="block-content block-content-full">
            <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
            <table id="tabela-rezervacije-na-cekanju" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                <thead>
                <tr>
                    <th class="text-center">Broj</th>
                    <th class="text-center">Vreme početka</th>
                    <th class="text-center">Vreme kraja</th>
                    <th class="d-none d-sm-table-cell">Ime i Prezime</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">E-Mail</th>
                    <th class="text-center" style="width: 15%;">Status</th>
                    <th class="text-center" style="width: 20%;">Akcija</th>
                </tr>
                </thead>
                <tbody>
                @foreach($rezervacijeNaCekanju as $rezervacija)
                    <tr>
                        <td class="text-center">{{$rezervacija->id}}</td>
                        <td class="text-center">{{$rezervacija->vreme_pocetka}}</td>
                        <td class="text-center">@if($rezervacija->vreme_kraja != null) {{$rezervacija->vreme_kraja}} @else / @endif</td>
                        <td class="font-w600">{{$rezervacija->klijent}}</td>
                        <td class="d-none d-sm-table-cell">{{$rezervacija->email}}</td>

                        <td class="d-none d-sm-table-cell text-center">
                                <span class="badge badge-warning">Na čekanju</span>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled rezervacije" href="/admin/rezervacija/{{$rezervacija->id}}">
                                <i class="si si-info"></i>
                            </a>


                            <form method="POST" style="display: inline;">
                                {{csrf_field()}}
                                <button type="submit" class="text-danger btn btn-sm btn-secondary" data-toggle="tooltip" title="Odbij" formaction="/admin/statusRezervacije/{{$rezervacija->id}}/odbijena">
                                    <i class="fa fa-times"></i>
                                </button>

                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <table id="tabela-potvrdjene-rezervacije" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display:none;">
                <thead>
                <tr>
                    <th class="text-center">Broj</th>
                    <th class="text-center">Vreme početka</th>
                    <th class="text-center">Vreme kraja</th>
                    <th class="d-none d-sm-table-cell">Ime i Prezime</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">E-Mail</th>
                    <th class="text-center" style="width: 15%;">Status</th>
                    <th class="text-center" style="width: 20%;">Akcija</th>
                </tr>
                </thead>
                <tbody>
                @foreach($potvrdjeneRezervacije as $rezervacija)
                    <tr>
                        <td class="text-center">{{$rezervacija->id}}</td>
                        <td class="text-center">{{$rezervacija->vreme_pocetka}}</td>
                        <td class="text-center">@if($rezervacija->vreme_kraja != null) {{$rezervacija->vreme_kraja}} @else / @endif</td>
                        <td class="font-w600">{{$rezervacija->klijent}}</td>
                        <td class="d-none d-sm-table-cell">{{$rezervacija->email}}</td>

                        <td class="d-none d-sm-table-cell text-center">
                            <span class="badge badge-success">Potvrđena</span>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled rezervacije" href="/admin/rezervacija/{{$rezervacija->id}}">
                                <i class="si si-info"></i>
                            </a>


                            <form method="POST" style="display: inline;">
                                {{csrf_field()}}

                                <button type="submit" class="text-warning btn btn-sm btn-secondary" data-toggle="tooltip" title="Vrati na čekanje" formaction="/admin/statusRezervacije/{{$rezervacija->id}}/na_cekanju">
                                    <i class="fa fa-flag"></i>
                                </button>

                                <button type="submit" class="text-danger btn btn-sm btn-secondary" data-toggle="tooltip" title="Odbij" formaction="/admin/statusRezervacije/{{$rezervacija->id}}/odbijena">
                                    <i class="fa fa-times"></i>
                                </button>


                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <table id="tabela-odbijene-rezervacije" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display: none;">
                <thead>
                <tr>
                    <th class="text-center">Broj</th>
                    <th class="text-center">Vreme početka</th>
                    <th class="text-center">Vreme kraja</th>
                    <th class="d-none d-sm-table-cell">Ime i Prezime</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">E-Mail</th>
                    <th class="text-center" style="width: 15%;">Status</th>
                    <th class="text-center" style="width: 20%;">Akcija</th>
                </tr>
                </thead>
                <tbody>
                @foreach($obrisaneOdbijeneRezervacije as $rezervacija)
                    <tr>
                        <td class="text-center">{{$rezervacija->id}}</td>
                        <td class="text-center">{{$rezervacija->vreme_pocetka}}</td>
                        <td class="text-center">@if($rezervacija->vreme_kraja != null) {{$rezervacija->vreme_kraja}} @else / @endif</td>
                        <td class="font-w600">{{$rezervacija->klijent}}</td>
                        <td class="d-none d-sm-table-cell">{{$rezervacija->email}}</td>

                        <td class="d-none d-sm-table-cell text-center">
                            <span class="badge badge-danger">@if($rezervacija->admin_kreirao) Obrisana @else Odbijena @endif</span>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled rezervacije" href="/admin/rezervacija/{{$rezervacija->id}}">
                                <i class="si si-info"></i>
                            </a>


                            <form method="POST" style="display: inline;">
                                {{csrf_field()}}
                                @if($rezervacija->admin_kreirao)
                                    <button type="submit" class="text-warning btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj" formaction="/admin/restaurirajRezervaciju/{{$rezervacija->id}}">
                                        <i class="fa fa-undo"></i>
                                    </button>
                                @else
                                    <button type="submit" class="text-warning btn btn-sm btn-secondary" data-toggle="tooltip" title="Vrati na čekanje" formaction="/admin/statusRezervacije/{{$rezervacija->id}}/na_cekanju">
                                        <i class="fa fa-flag"></i>
                                    </button>
                                @endif
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <!-- END Overview -->
    <!-- END Overview -->
    <form id="forma-opcija" method="POST" >
        {{csrf_field()}}
        <!-- Update Product -->
        <h2 class="content-heading">Kalendar</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-12">
                <div id="calendar"></div>
            </div>
        </div>

            <!-- END Basic Info -->

        <!-- END Update Product -->
        <input type="submit" id="forma-opcija-submit-button" style="display:none"/>
    </form>


@stop