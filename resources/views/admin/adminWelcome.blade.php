@extends('admin.adminLayout')

@section('title')
Naslovna
@stop

@section('breadcrumbs')
<span class="breadcrumb-item active">Admin</span>
@stop



@section('heder-h1')
Admin - Maderoterapije
@stop


@section('heder-h2')
Trenutno imate <a class="text-primary-light link-effect" href="/admin/porudzbine">{{count($novePorudzbine)}} novih porudžbina</a>.
@stop

@section('scriptsBottom')
    <script src="{{asset('assets/js/pages/be_pages_ecom_dashboard.js')}}"></script>
    <script>inicijalizujKontrolnuTablu('{!! addslashes(json_encode($datumi)) !!}', '{!! addslashes(json_encode($brojeviPorudzbina)) !!}', '{!! addslashes(json_encode($iznosi)) !!}');</script>
@endsection

@section('main')

<div class="row gutters-tiny">
    <!-- Earnings -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-transparent bg-gd-elegance" href="/admin/porudzbine">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-cart-arrow-down text-white-op"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">PORUDŽBINE</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Earnings -->

    <!-- Orders -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-transparent bg-gd-dusk" href="/admin/proizvodi">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-archive text-white-op"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white" >PROIZVODI</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Orders -->

    <!-- New Customers -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-transparent bg-gd-sea" href="admin/fakture">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-file text-white-op"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">FAKTURE</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END New Customers -->

    <!-- Conversion Rate -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-transparent bg-gd-aqua" href="admin/izvestaji">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-area-chart text-white-op"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-white">IZVEŠTAJI</div>
                    <div class="font-size-sm font-w600 text-uppercase text-white-op"></div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Conversion Rate -->
</div>
<!-- END Statistics -->
<!-- Latest Orders and Top Products -->
<div class="row gutters-tiny">
    <!-- Latest Orders -->
    <div class="col-xl-6">
        <h2 class="content-heading">Nove porudžbine</h2>
        <div class="block block-rounded">
            <div class="block-content">
                <table class="table table-borderless table-striped">
                    <thead>
                        <tr>
                            <th style="width: 100px;">Broj</th>
                            <th>Datum</th>
                            <th class="d-none d-sm-table-cell">Ime i prezime</th>
                            <th class="text-right">Iznos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($novePorudzbine as $porudzbina)
                        <tr>
                            <td>
                                <a class="font-w600" href="/admin/porudzbina/{{$porudzbina->id}}">{{$porudzbina->id}}</a>
                            </td>
                            <td>
                                <span>{{date('d.m.Y.', strtotime($porudzbina->created_at))}}</span>
                            </td>
                            <td class="d-none d-sm-table-cell">
                                @if($porudzbina->id_user != null)
                                    <a href="/admin/korisnik/{{$porudzbina->id_user}}">{{$porudzbina->kupac}}</a>
                                @else
                                    {{$porudzbina->kupac}}
                                @endif

                            </td>
                            <td class="text-right">
                                <span class="text-black">{{number_format($porudzbina->iznos_popust, 2, ',', '.')}} rsd</span>
                            </td>
                        </tr>
                        @endforeach

                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Latest Orders -->

    <!-- Top Products -->
    <div class="col-xl-6">
        <h2 class="content-heading">Najprodavaniji proizvodi</h2>
        <div class="block block-rounded">
            <div class="block-content">
                <table class="table table-borderless table-striped">
                    <thead>
                        <tr>
                            <th class="d-none d-sm-table-cell" style="width: 100px;">Šifra</th>
                            <th>Proizvod</th>
                            <th class="text-center">Cena</th>
                            <th class="d-none d-sm-table-cell text-center">Broj prodatih</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($najprodavanijiProizvodi as $proizvod)
                        <tr>
                            <td class="d-none d-sm-table-cell">
                                <a class="font-w600" href="/admin/proizvod/{{$proizvod->id}}">{{$proizvod->id}}</a>
                            </td>
                            <td>
                                <a href="/admin/proizvod/{{$proizvod->id}}">{{$proizvod->naziv}}</a>
                            </td>
                            <td class="text-center">
                                <a class="text-gray-dark" href="/admin/proizvod/{{$proizvod->id}}">@if($proizvod->na_popustu) {{number_format($proizvod->cena_popust, 2, ',', '.')}} rsd @else {{number_format($proizvod->cena, 2, ',', '.')}} rsd @endif</a>
                            </td>
                            <td class="text-center">
                                <a class="text-gray-dark" href="/admin/proizvod/{{$proizvod->id}}">{{$proizvod->broj_porucivanja}}</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END Top Products -->
</div>
<!-- END Latest Orders and Top Products -->
<div class="row gutters-tiny">


    <!-- Orders Volume Chart -->
    <div class="col-md-6">
        <div class="block block-rounded block-mode-loading-refresh">
            <div class="block-header">
                <h3 class="block-title">
                    PORUDŽBINE za poslednjih 7 dana
                </h3>
                
            </div>
            <div class="block-content block-content-full bg-body-light text-center">
                <div class="row gutters-tiny">
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno</div>
                        <div class="font-size-h3 font-w600">{{$brojStorniranih + $brojNestorniranih}}</div>
                    </div>
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Uspešnih</div>
                        <div class="font-size-h3 font-w600 text-success">{{$brojNestorniranih}}</div>
                    </div>
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Storniranih</div>
                        <div class="font-size-h3 font-w600 text-danger">{{$brojStorniranih}}</div>
                    </div>
                </div>
            </div>
            <div class="block-content block-content-full">
                <div class="pull-all">
                    <!-- Orders Chart Container -->
                    <canvas class="js-chartjs-ecom-dashboard-orders"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- END Orders Volume Chart -->
    <!-- Orders Earnings Chart -->
    <div class="col-md-6">
        <div class="block block-rounded block-mode-loading-refresh">
            <div class="block-header">
                <h3 class="block-title">
                    PROMET za poslednjih 7 dana
                </h3>
            </div>
            <div class="block-content block-content-full bg-body-light text-center">
                <div class="row gutters-tiny">
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno</div>
                        <div class="font-size-h3 font-w600">{{number_format($ukupanIznos, 2, ',', '.')}}</div>
                    </div>
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Prosečno po porudžbini</div>
                        <div class="font-size-h3 font-w600 text-success">{{number_format($prosecanIznos, 2, ',', '.')}}</div>
                    </div>
                    <div class="col-4">
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Najveća porudžbina</div>
                        <div class="font-size-h3 font-w600 text-danger">{{number_format($maksimalanIznos, 2, ',', '.')}}</div>
                    </div>
                </div>
            </div>
            <div class="block-content block-content-full">
                <div class="pull-all">
                    <!-- Earnings Chart Container -->
                    <canvas class="js-chartjs-ecom-dashboard-earnings"></canvas>
                </div>
            </div>
        </div>
    </div>
    <!-- END Orders Earnings Chart -->
</div>
<!-- END Orders Overview -->
@stop