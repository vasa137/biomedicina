@extends('admin.adminLayout')

@section('title')
    Usluge
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <span class="breadcrumb-item active">Usluge</span>
@stop

@section('heder-h1')
    Usluge
@stop


@section('heder-h2')
    Trenutno <a class="text-primary-light link-effect">{{$brojAktivnihUsluga}} aktivnih usluga</a>.
@stop

@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaUsluge.js')}}"></script>
@endsection

@section('main')
    <div class="row gutters-tiny">
        <!-- All Products -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-circle-o fa-2x text-info-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{count($usluge)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno usluga</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END All Products -->

        <!-- Top Sellers -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" >
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-star fa-2x text-warning-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($grupeUsluga)}}">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno grupa usluga</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Top Sellers -->

        <!-- Add Product -->
        <div class="col-md-6 col-xl-3">
            <a class="block block-rounded block-link-shadow" href="/admin/usluga/-1">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-archive fa-2x text-success-light"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-plus"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Dodaj novu uslugu</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Add Product -->
    </div>
    <!-- END Overview -->
    @foreach($grupeUsluga as $grupaUsluga)
        <!-- Dynamic Table Full Pagination -->
        <div class="block">

            <div class="block-header block-header-default">
                <h3 id="grupa-usluga-{{$grupaUsluga->id}}-title" class="block-title">{{$grupaUsluga->naziv}}</h3>
            </div>

            <div class="block-content block-content-full">


                <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
                <table id="tabela-grupeUsluga-{{$grupaUsluga->id}}" class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                    <tr>
                        <th style="width:40%;">Naziv</th>
                        <th class="d-none d-sm-table-cell text-center" style="width:20%;">Različitih proizvoda</th>
                        <th class="d-none d-sm-table-cell text-center" style="width:20%;">Status</th>
                        <th class="text-center" style="width:20%;">Akcija</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($grupaUsluga->usluge as $usluga)
                        <tr>
                            <td class="font-w600">{{$usluga->naziv}}</td>
                            <td class="d-none d-sm-table-cell text-center">{{$usluga->broj_rezervacija}}</td>
                            <td class="d-none d-sm-table-cell text-center"><span @if(!$usluga->sakriven) class="text-success" @else class="text-danger" @endif> @if(!$usluga->sakriven) Aktivna @else Obrisana @endif</span></td>

                            <td class="text-center">
                                <a href="/admin/usluga/{{$usluga->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Izmeni uslugu">
                                    <i class="fa fa-edit"></i>
                                </a>

                                @if(!$usluga->sakriven)
                                    <form method="POST" action="/admin/obrisiUslugu/{{$usluga->id}}" style="display:inline">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Obriši uslugu">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>
                                @else
                                    <form method="POST" action="/admin/restaurirajUslugu/{{$usluga->id}}" style="display:inline">
                                        {{csrf_field()}}
                                        <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Restauriraj uslugu">
                                            <i class="fa fa-undo"></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>




            </div>

        </div>
    @endforeach
@stop