@extends('admin.adminLayout')

@section('title')
Korisnici
@stop

@section('breadcrumbs')
<a class="breadcrumb-item" href="/admin">Admin</a>
<span class="breadcrumb-item active">Korisnici</span>
@stop

@section('heder-h1')
    Korisnici
@stop


@section('heder-h2')
    Trenutno <a class="text-primary-light link-effect">{{count($korisnici)}} aktivnih korisnika</a>.
@stop


@section('scriptsTop')
    <script src="{{asset('/js/adminKorisnici.js')}}"></script>
@endsection
@section('scriptsBottom')
    <!-- Page JS Code -->
    <script src="{{asset('/js/tabelaKorisnici.js')}}"></script>
@endsection



@section('main')
<div class="row gutters-tiny">
    <!-- All Products -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" >
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-user fa-2x text-warning-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-warning" data-toggle="countTo" data-to="{{count($korisnici) + count($korisniciBlokirani)}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Ukupno Korisnika</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END All Products -->


    <!-- Add Product -->
    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="javascript:prikaziAktivne()">
            <div class="block-content block-content-full block-sticky-options">
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-user fa-2x text-success-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-success" data-toggle="countTo" data-to="{{count($korisnici)}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Aktivni</div>
                </div>
            </div>
        </a>
    </div>

    <div class="col-md-6 col-xl-3">
        <a class="block block-rounded block-link-shadow" href="javascript:prikaziBlokirane()">
            <div class="block-content block-content-full block-sticky-options" >
                <div class="block-options">
                    <div class="block-options-item">
                        <i class="fa fa-user fa-2x text-danger-light"></i>
                    </div>
                </div>
                <div class="py-20 text-center">
                    <div class="font-size-h2 font-w700 mb-0 text-danger" data-toggle="countTo" data-to="{{count($korisniciBlokirani)}}">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Blokirani</div>
                </div>
            </div>
        </a>
    </div>
    <!-- END Add Product -->
</div>
<!-- END Overview -->

<!-- Dynamic Table Full Pagination -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 id="kupci-title" class="block-title">Aktivni korisnici</h3>
    </div>
    <div class="block-content block-content-full">
        <!-- DataTables init on table by adding .js-dataTable-full-pagination class, functionality initialized in js/pages/be_tables_datatables.js -->
        <table id="aktivni-korisnici-tabela" class="table table-bordered table-striped table-vcenter js-dataTable-full">
            <thead>
                <tr>
                    <th>Ime i Prezime</th>
                    <th>E-Mail</th>
                    <th class="d-none d-sm-table-cell">Promet</th>
                    <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Broj porudžbina</th>
                    <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Broj proizvoda</th>
                    <th class="text-center" style="width: 15%;">Akcija</th>
                </tr>
            </thead>
            <tbody>
                @foreach($korisnici as $korisnik)
                    <tr>
                        <td>{{$korisnik->ime_prezime}}</td>
                        <td class="font-w600"><a href="mailto:{{$korisnik->email}}">{{$korisnik->email}}</a></td>
                        <td class="d-none d-sm-table-cell">{{number_format($korisnik->promet, 2, ',', '.')}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$korisnik->broj_porudzbina}}</td>
                        <td class="d-none d-sm-table-cell text-center">{{$korisnik->broj_porucenih_proizvoda}}</td>

                        <td class="text-center">
                            <a href="/admin/korisnik/{{$korisnik->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled/izmena korisnika">
                                <i class="fa fa-user"></i>
                            </a>
                            <form method="POST" action="/admin/blokirajKorisnika/{{$korisnik->id}}" style="display: inline">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Blokiraj korisnika">
                                    <i class="fa fa-ban"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                
               
            </tbody>
        </table>

        <table id="blokirani-korisnici-tabela" class="table table-bordered table-striped table-vcenter js-dataTable-full" style="display: none;">
            <thead>
            <tr>
                <th>Ime i Prezime</th>
                <th>E-Mail</th>
                <th class="d-none d-sm-table-cell">Promet</th>
                <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Broj porudžbina</th>
                <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Broj proizvoda</th>
                <th class="text-center" style="width: 10%;">Akcija</th>
            </tr>
            </thead>
            <tbody>
            @foreach($korisniciBlokirani as $korisnik)
                <tr>
                    <td >{{$korisnik->ime_prezime}}</td>
                    <td class="font-w600"><a href="mailto:{{$korisnik->email}}">{{$korisnik->email}}</a></td>
                    <td class="d-none d-sm-table-cell">{{number_format($korisnik->promet, 2, ',', '.')}}</td>
                    <td class="d-none d-sm-table-cell text-center">{{$korisnik->broj_porudzbina}}</td>
                    <td class="d-none d-sm-table-cell text-center">{{$korisnik->broj_porucenih_proizvoda}}</td>

                    <td class="text-center">
                        <a href="/admin/korisnik/{{$korisnik->id}}" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Pregled/izmena korisnika">
                            <i class="fa fa-user"></i>
                        </a>
                        <form method="POST" action="/admin/odblokirajKorisnika/{{$korisnik->id}}" style="display: inline">
                            {{csrf_field()}}
                            <button type="submit" class="btn btn-sm btn-secondary" data-toggle="tooltip" title="Odblokiraj korisnika">
                                <i class="fa fa-check-circle"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach



            </tbody>
        </table>
    </div>
</div>
<!-- END Dynamic Table Full Pagination -->
@stop