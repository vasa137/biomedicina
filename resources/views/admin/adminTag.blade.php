@extends('admin.adminLayout')

@section('title')
    @if($izmena)
        Tag - {{$tag->naziv}}
    @else
        Novi tag
    @endif
@stop

@section('breadcrumbs')
    <a class="breadcrumb-item" href="/admin">Admin</a>
    <a class="breadcrumb-item" href="/admin/tagovi">Tagovi</a>
    <span class="breadcrumb-item active">@if($izmena){{$tag->naziv}} @else Novi tag @endif</span>
@stop

@section('heder-h1')
    @if($izmena){{$tag->naziv}} @else Novi tag @endif
@stop



@section('main')
    <div class="row gutters-tiny">
    @if($izmena)

        <!-- In Orders -->
            <div class="col-md-3 col-xl-3">
                <a class="block block-rounded block-link-shadow" >
                    <div class="block-content block-content-full block-sticky-options">
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa fa-shopping-basket fa-2x text-info"></i>
                            </div>
                        </div>
                        <div class="py-20 text-center">
                            <div class="font-size-h2 font-w700 mb-0 text-info" data-toggle="countTo" data-to="{{$tag->broj_proizvoda}}">0</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Različitih proizvoda</div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- END In Orders -->
    @endif
    <!-- Stock -->
        <div class="col-md-3 col-xl-3">

            <a class="block block-rounded block-link-shadow" href="javascript:$('#forma-tag-submit-button').click()">
                <div class="block-content block-content-full block-sticky-options">
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="si si-settings fa-2x text-success"></i>
                        </div>
                    </div>
                    <div class="py-20 text-center">
                        <div class="font-size-h2 font-w700 mb-0 text-success">
                            <i class="fa fa-check"></i>
                        </div>
                        <div class="font-size-sm font-w600 text-uppercase text-muted">Sačuvaj</div>
                    </div>
                </div>
            </a>
        </div>
        <!-- END Stock -->

    @if($izmena)
        @if(!$tag->sakriven)
            <!-- Delete Product -->
                <div class="col-md-3 col-xl-3">
                    <form id="forma-obrisi-tag" method="POST" action="/admin/obrisiTag/{{$tag->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-obrisi-tag').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-trash fa-2x text-danger"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-danger">
                                        <i class="fa fa-times"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Obriši tag</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>

            @else
                <div class="col-md-3 col-xl-3">
                    <form id="forma-restauriraj-tag" method="POST" action="/admin/restaurirajTag/{{$tag->id}}">
                        {{csrf_field()}}
                        <a class="block block-rounded block-link-shadow" href="javascript:document.getElementById('forma-restauriraj-tag').submit();">
                            <div class="block-content block-content-full block-sticky-options">
                                <div class="block-options">
                                    <div class="block-options-item">
                                        <i class="fa fa-lightbulb-o fa-2x text-warning"></i>
                                    </div>
                                </div>
                                <div class="py-20 text-center">
                                    <div class="font-size-h2 font-w700 mb-0 text-warning">
                                        <i class="fa fa-undo"></i>
                                    </div>
                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Restauriraj tag</div>
                                </div>
                            </div>
                        </a>
                    </form>
                </div>
        @endif
    @endif
    <!-- END Delete Product -->
    </div>
    <!-- END Overview -->
    <form id="forma-tag" method="POST" @if($izmena) action="/admin/sacuvajTag/{{$tag->id}}" @else action="/admin/sacuvajTag/-1" @endif>
    {{csrf_field()}}
    <!-- Update Product -->
        <h2 class="content-heading">Informacije o tagu</h2>
        <div class="row gutters-tiny">
            <!-- Basic Info -->
            <div class="col-md-12">
                <div class="block block-rounded block-themed">
                    <div class="block-header bg-gd-primary">
                        <h3 class="block-title">Informacije</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group row">
                            <label class="col-12" >Naziv</label>
                            <div class="col-12 input-group">
                                <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i class="si si-info"></i>
                                </span>
                                </div>
                                <input maxlength="254" type="text" class="form-control" name="naziv" @if($izmena) value="{{$tag->naziv}}" @endif required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-12">Opis</label>
                            <div class="col-12">
                                <!-- CKEditor (js-ckeditor id is initialized in Codebase() -> uiHelperCkeditor()) -->
                                <!-- For more info and examples you can check out http://ckeditor.com -->
                                <textarea maxlength="15999" class="form-control" name="opis" rows="8">@if($izmena){{$tag->opis}}@endif</textarea>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <!-- END Basic Info -->



        </div>
        <!-- END More Options -->

        <!-- END Update Product -->
        <input type="submit" id="forma-tag-submit-button" style="display:none"/>
    </form>
@stop