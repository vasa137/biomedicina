
@foreach($proizvodi as $proizvod)
    <div class="col-lg-4 col-sm-6">
        <div class="single-new-collection-item">
            <!-- single new collections -->
            <div class="thumb">
                @if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike .'.jpg')))
                    <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                        <img class="img-fluid" src="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg" alt="<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>">
                    </a>
                @endif
                <!--
                <div class="hover">
                    <a onclick="dodajUKorpu('{!! $proizvod->id !!}', 1);" style="cursor:pointer;" class="addtocart">Dodaj u korpu</a>
                </div>
            -->
            </div>
            <div class="content">
                <a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $proizvod->naziv))?>/{{$proizvod->id}}">
                    <h4 class="title">{{$proizvod->naziv}}</h4>
                </a>
                <div class="price">
                    @if($proizvod->na_popustu)
                        <span class="sprice">{{number_format($proizvod->cena_popust, 0, ',', '.')}} RSD</span>
                        <del class="dprice">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</del>
                    @else
                        <span class="sprice">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
                    @endif
                </div>
            </div>
        </div><!-- //. single new collections  -->
    </div>
@endforeach