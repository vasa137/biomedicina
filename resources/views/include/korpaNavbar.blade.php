<div class="shopping-cart">
    @if(Cart::instance('korpa')->count() == 0)
        Korpa je prazna.
    @else
        <div class="shopping-cart-header">
            <i class="fa fa-shopping-cart cart-icon"></i><span class="badge">{{Cart::instance('korpa')->count()}}</span>
            <div class="shopping-cart-total">
                <span class="lighter-text">Ukupno:</span>
                <span class="main-color-text">{{Cart::instance('korpa')->total(0,',', '.')}} rsd</span>
            </div>
        </div> <!--end shopping-cart-header -->

        <ul class="shopping-cart-items">
            @foreach(Cart::instance('korpa')->content() as $stavka)
                <li class="clearfix">
                    @if(File::exists(public_path('/images/proizvodi/' .$stavka->id  . '/glavna/' . $stavka->nazivGlavneSlike . '.jpg')))

                        <img width="70" height="70" src="{{asset('images/proizvodi/'. $stavka->id .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}"/>

                    @endif
                    <span class="item-name">{{$stavka->name}}</span>
                    <span class="item-price">{{number_format($stavka->price - $stavka->tax, 0, ',', '.')}} rsd</span>
                    <span class="item-quantity">Kol: {{$stavka->qty}}</span>
                </li>
            @endforeach

        </ul>

        <a href="/korpa" id="pregled-button">Pregled korpe</a>
    @endif
</div>
