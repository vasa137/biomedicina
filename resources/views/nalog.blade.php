@extends('layout')

@section('title')
{{$user->ime_prezime}} - Korisnički Nalog -
@stop


@section('scriptsTop')
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
        <div class="bcoverlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="Content">
                        <h2>
                            {{$user->ime_prezime}} - Korisnički Nalog
                        </h2>
                        <div class="links">
                            <ul>
                                <li>
                                    <a href="/">Naslovna</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    <a class="active" href="/nalog">{{$user->ime_prezime}}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Welcome Area End -->

    <!-- SignUP Area Start -->
    <section class="logRegForm">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-md-12">
                    <div class="contact_form_wrappre2">
                            @if (Session::has('success'))
                                 <p class="offset-4 col-7" style="color:green; font-weight:bold;">{{ Session::get('success') }}</p>
                            @elseif($errors->has('greska'))
                                <p class="offset-4 col-7" style="color:red; font-weight:bold;">{{$errors->first('greska')}}</p>
                            @endif
                            <div class="inputArea">
                                <form method="POST" action="/promeni_licne_podatke">
                                    @csrf
                                    
                                    <br/>
                                <div class="form-row">

                                    <div class="col-6">
                                        <div class="input-group">
                                            <input maxlength="254" id="name" type="text" class="form-control{{ $errors->has('ime_prezime') ? ' is-invalid' : '' }}" name="ime_prezime" value="{{$user->ime_prezime}}" placeholder="Ime i prezime*" aria-describedby="username" required autofocus>

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="username">
                                                    <i class="fas fa-user"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('ime_prezime'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('ime_prezime') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input maxlength="254" disabled id="email" type="email" class="form-control" name="email" placeholder="Email*" value="{{ $user->email }}" aria-describedby="Site">


                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="Site">
                                                    <i class="fas fa-envelope"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="col-6">
                                        <div class="input-group">
                                            <input maxlength="254" id="adresa" type="text" class="form-control{{ $errors->has('adresa') ? ' is-invalid' : '' }}" name="adresa" placeholder="Adresa" value="{{$user->adresa}}" aria-describedby="Site">


                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="Site">
                                                    <i class="fas fa-home"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('adresa'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('adresa') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input maxlength="19" id="telefon" type="text" class="form-control{{ $errors->has('telefon') ? ' is-invalid' : '' }}" name="telefon" placeholder="Telefon" value="{{$user->telefon}}" aria-describedby="Site">


                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="Site">
                                                    <i class="fas fa-phone"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('telefon'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('telefon') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">

                                    <div class="col-6">
                                        <div class="input-group">
                                            <input maxlength="254" id="grad" type="text" class="form-control{{ $errors->has('grad') ? ' is-invalid' : '' }}" name="grad" placeholder="Grad" value="{{$user->grad}}" aria-describedby="Site">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="Site">
                                                    <i class="fas fa-city"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('grad'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('grad') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input min="10000" max="99999" id="zip" type="number" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="zip" placeholder="Poštanski Broj" value="{{$user->zip}}" aria-describedby="Site">

                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="Site">
                                                    <i class="fas fa-barcode"></i>
                                                </span>
                                            </div>
                                            @if ($errors->has('zip'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('zip') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="form-row">
                                    <div class="col-md-12">
                                        <button class="lostpass" type="submit">Promeni podatke</button>
                                        <br>
                                    </div>
                                </div>
                                </form>
                                <form method="POST" action="/promeni_lozinku">
                                    @csrf
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input maxlength="254" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Lozinka*" aria-describedby="url" required>
                                            <div class="input-group-prepend">
														<span class="input-group-text" id="url">
															<i class="fas fa-key"></i>
														</span>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <div class="input-group">
                                            <input maxlength="254" id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Potvrda lozinke*" aria-describedby="url" required>
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="url">
                                                    <i class="fas fa-key"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="form-row">
                                    <div class="col-md-12">
                                        <button type="submit" class="loginnow">Promeni lozinku</button>
                                    </div>
                                </div>
                                
                            </form>
                               
                            </div>
                            <br><br>
                            <!--PORUDZBINE POCETAK-->
                            <div class="sectionTheading">
                            	<h2>Moje Porudžbine:</h2>
                            </div>
                            <br>

                            @foreach($porudzbine as $porudzbina)
							<table>

							<tr>
							    <th style="text-align: center;" colspan="3"><i class="fas fa-file"></i> BROJ PORUDŽBINE: {{$porudzbina->id}}</th>
							    <th style="text-align: center;"><i class="fas fa-calendar"></i> DATUM: {{$porudzbina->created_at}}</th>
							    <th style="text-align: center;"><i class="fas fa-sync "></i> STATUS: <span style="text-transform: capitalize;" >{{$porudzbina->status}} </span></th>
							  </tr>
							  <tr>
							    <th>Proizvod</th>
							    <th style="text-align: center;">Količina</th>
							    <th style="text-align: right;">Cena</th>
                                  <th style="text-align: right;">Kupon</th>
							    <th style="text-align: right;">Ukupno</th>
							  </tr>
                                @foreach($porudzbina->stavke as $stavka)
                                  <tr>
                                    <td >{{$stavka->proizvod->naziv}}</td>
                                    <td style="text-align: center;">{{$stavka->kolicina}}</td>
                                    <td style="text-align: right;">{{number_format($stavka->cena_popust, 2, ',', '.')}} rsd</td>
                                      <td style="text-align: right;">@if($stavka->id_kupon == null) / @else {{$stavka->kupon->kod}} ({{$stavka->kupon->popust*100}}%) @endif</td>
                                    <td style="text-align: right;">{{number_format($stavka->ukupno_popust, 2, ',', '.')}} rsd</td>
                                  </tr>
                                @endforeach
                                @if($porudzbina->ima_vaucere)
                                    @foreach($porudzbina->vauceri as $vaucer)
                                      <tr>
                                          <td ></td>
                                          <td style="text-align: center;"></td>
                                          <td style="text-align: right;"></td>
                                          <td style="text-align: right;">Vaučer {{$vaucer->kod}}</td>
                                          <td style="text-align: right;">-{{number_format($vaucer->iznos, 2, ',', '.')}} rsd</td>
                                      </tr>
                                    @endforeach
                                @endif
							  <tr>
							    <td style="text-align: right;" colspan="4"><strong>UKUPNO</strong></td>
							    <td style="text-align: right;"><strong>{{number_format($porudzbina->iznos_popust,2, ',', '.')}} rsd</strong></td>
							  </tr>
							</table>
							<br/>
                                <br/>
                                <hr>
                                <br/>
                                <br/>
							@endforeach
                            <!--PORUDZBINE POCETAK-->
                                <div class="sectionTheading">
                                    <h2>Moje Rezervacije:</h2>
                                </div>
                                <br>
                                @foreach($rezervacije as $rezervacija)
                                    <table>

                                        <tr>
                                            <th style="text-align: center;" colspan="3"><i class="fas fa-file"></i> BROJ REZERVACIJE: {{$rezervacija->id}}</th>
                                            <th style="text-align: center;"><i class="fas fa-calendar"></i> DATUM: {{$rezervacija->created_at}}</th>
                                            <th style="text-align: center;"><i class="fas fa-sync "></i> STATUS:
                                                <span style="text-transform: capitalize;" >
                                                    @if($rezervacija->status == 'na_cekanju')
                                                        Na čekanju
                                                    @else
                                                        {{$rezervacija->status}}
                                                    @endif
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Usluga</th>
                                            <th>Klijent</th>
                                            <th style="text-align: center;">Vreme početka</th>
                                            <th style="text-align: center;">Vreme kraja</th>
                                            <th style="text-align: justify;">Napomena</th>
                                        </tr>

                                        <tr>
                                            <td >{{$rezervacija->usluga->naziv}}</td>
                                            <td >{{$rezervacija->klijent}}</td>
                                            <td style="text-align: center;">{{date('d.m.Y H:i',strtotime($rezervacija->vreme_pocetka))}}</td>
                                            <td style="text-align: center;">@if($rezervacija->vreme_kraja != null) {{date('d.m.Y H:i',strtotime($rezervacija->vreme_kraja))}} @else Na čekanju @endif</td>
                                            <td style="text-align: justify;">{{$rezervacija->napomena}}</td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <hr>
                                    <br/>
                                    <br/>
                                @endforeach
                    </div>
                </div>
            </div>
        </div>


    </section>
    <!-- SignUp Area End -->

@stop