@extends('layout')

@section('scriptsTop')
    <script src="{{asset('js/rezervacija.js')}}"></script>
@endsection
@section('sekcije')
    <section id="spabreadcrumb" class="spabreadcrumb">
        <div class="bcoverlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="Content">
                        <h2>
                            Rezervacija termina
                        </h2>
                        <div class="links">
                            <ul>
                                <li>
                                    <a href="/">Naslovna</a>
                                </li>
                                <li>
                                    <span>/</span>
                                </li>
                                <li>
                                    <a class="active" href="/rezervacija">Rezervacija termina</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Welcome Area End -->

    <!--Start Contact Wrap-->
    <div class="contact-wrap">
        <!--Start Container-->
        <div class="container">
            <!--Contact Map-->
            <!--Start Row-->
            <div class="row">
                <div class="col-md-4 d-flex align-self-center">
                    <div class="address">
                        <h4>Essence of Beauty</h4>
                        <h3>Informacije</h3>
                        <p class="a-text">Edukacije i proizvodnja opreme za maderoterapiju.</p>
                        <div class="media">
                            <div class="left align-self-center mr-3">
                                <i class="pe-7s-map-marker"></i>
                            </div>
                            <div class="media-body">
                                <h4>Adresa</h4>
                                <p>Bulevar Zorana Đinđića 6, 11000 Beograd</p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="left align-self-center mr-3">
                                <i class="pe-7s-call"></i>
                            </div>
                            <div class="media-body">
                                <h4>Telefon</h4>
                                <p>+381 69 4559099</p>
                            </div>
                        </div>
                        <div class="media">
                            <div class="left align-self-center mr-3">
                                <i class="pe-7s-mail"></i>
                            </div>
                            <div class="media-body">
                                <h4>E-mail</h4>
                                <p>info@essenceofbeauty.rs</p>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <!--Start Contact Form-->
                    <div class="contact-form">
                        <h2>Rezervišite termin</h2>
                        <p>Rezervišite termin željenog tretmana i sačekajte na potvrdu od strane našeg tima.</p>
                        <form action="/rezervisi" id="kontakt_forma" method="POST" onsubmit="return validirajVreme();">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-element">
                                        <input type="text" maxlength="254" class="form-control input-field" id="ime_prezime" name="ime_prezime" placeholder="Ime i prezime*" @if(Auth::check()) value="{{Auth::user()->ime_prezime}}" @endif required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-element">
                                        <input type="email" maxlength="254" class="form-control input-field" id="email" name="email" placeholder="E-mail*" @if(Auth::check()) value="{{Auth::user()->email}}" @endif required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-element">
                                        <input type="text" maxlength="19" class="form-control input-field" id="telefon" name="telefon" placeholder="Telefon*" @if(Auth::check()) value="{{Auth::user()->telefon}}" @endif required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Tretman</label>
                                    <div class="form-group form-element">
                                        <select class="form-control input-field" name="id_usluga" required>
                                            @foreach($usluge as $usluga)
                                                <option value="{{$usluga->id}}">{{$usluga->naziv}} ({{number_format($usluga->iznos, 0, '', '.')}} rsd)</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Datum</label>
                                    <div class="form-group form-element">
                                        <input onchange="proveriRezervacije()" id="datum" type="date" class="form-control input-field" name="datum" placeholder="Datum*" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label>Željeno vreme početka</label>
                                    <div class="form-group form-element">
                                        <select onchange="proveriRezervacije()" id="vreme_pocetka" class="form-control input-field" id="name" name="vreme_pocetka" required>
                                            <?php for($i = 8; $i <= 19; $i++) { ?>
                                                @if($i < 10)
                                                    <option value="{{$i*60}}">0{{$i}}:00</option>
                                                    <option value="{{$i*60 + 30}}">0{{$i}}:30</option>
                                                @else
                                                    <option value="{{$i* 60}}">{{$i}}:00</option>
                                                    <option value="{{$i * 60 + 30}}">{{$i}}:30</option>
                                                @endif
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group form-element">
                                        <textarea maxlength="999" id="message" cols="30" rows="10" class="form-control" name="napomena" placeholder="Napomena"></textarea>
                                    </div>
                                </div>
                            </div>

                            <p style="color:red" id="errorMessage"></p>
                            <p style="color:green" id="successMessage"></p>
                            <div class="contact-frm-btn">
                                <button type="submit" form="kontakt_forma" value="Submit" class="mr_btn_fill">Pošaljite upit </button>
                            </div>
                        </form>
                    </div>
                    <!--End Contact Form-->
                </div>
            </div>
            <!--End Row-->
        </div>
        <!--End Container-->
    </div>
    <!--End Contact Wrap-->
    <!--End Page Content-->
    <!-- google map area start -->

    <div class="map-wrapper">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3365.976151895971!2d20.429736156759425!3d44.81265552139742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a655f2b30c7a5%3A0xa32ea7144201f71a!2sBulevar+Zorana+%C4%90in%C4%91i%C4%87a+6%2C+Beograd+11070!5e0!3m2!1sen!2srs!4v1554244102906!5m2!1sen!2srs" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <!-- google map area end -->
@stop