@extends('layout')


@section('title')
Korpa - 
@stop

@section('scriptsTop')
	<link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
@endsection

@section('scriptsBottom')
	<script src="{{asset('js/klijentKorpa.js')}}"></script>
	<script src="{{asset('js/popupDialog.js')}}"></script>
@endsection

@section('sekcije')

<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Korpa
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/korpa">Korpa</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->

<!-- Cart Sectin Srart -->

<div class="cart-content-area">
	<div class="container">
		<div class="row">
			<div id="korpaPrikaz" class="col-lg-12">
				@if(count($stavke) == 0)
					<h3>Korpa je prazna.</h3>
				@else
					<div class="cart-content-inner"><!-- cart content inner -->
					<div class="top-content"><!-- top content -->
						<div class="table-responsive" id="tabele-div" >
							<table class="table">
								<thead>
									<tr>
										<th>Proizvod</th>
										<th>Kupon</th>
										<th>Cena</th>
										<th>Količina</th>
										<th>Ukupno</th>
									</tr>
								</thead>
								<tbody id="tabela-korpa">
									@foreach($stavke as $stavka)
									<tr id="stavka-proizvod-{{$stavka->rowId}}">
										@include('include.popupDialog', ['poruka' => 'Da li ste sigurni da želite da obrišete proizvod iz korpe?', 'linkUspesno' => 'javascript:obrisiIzKorpe(\''. $stavka->rowId . '\',\'' . $stavka->id .'\')', 'dialogId' => $stavka->rowId])
										<td>
											<div class="product-details"><!-- product details -->
													<div class="close-btn cart-remove-item" onclick="otvoriDialogSaId('{!! $stavka->rowId!!}')">

															<i class="fas fa-times" ></i>
													</div>
													<div class="thumb">

														@if(File::exists(public_path('/images/proizvodi/' .$stavka->id  . '/glavna/' . $stavka->nazivGlavneSlike . '.jpg')))
															<a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->nazivGlavneSlike))?>/{{$stavka->id}}">
																<img height="160" width="160" src="{{asset('/images/proizvodi/'. $stavka->id .'/glavna/' . $stavka->nazivGlavneSlike . '.jpg')}}" alt="cart image">
															</a>
														@endif

													</div>
													<div class="content">
														<a href="/proizvod/<?=str_replace('?', '',str_replace(' ', '-', $stavka->name))?>/{{$stavka->id}}">
															<h4 class="title">{{$stavka->name}}</h4>
														</a>
													</div>
											</div><!-- //. product detials -->
										</td>
										<td >
											<div style="margin-left:15px;" class="price" id="kupon-stavka-{{$stavka->rowId}}">@if($stavka->kupon != null) {{$stavka->kupon->kod}} ({{$stavka->kupon->popust*100}}%) @else / @endif</div>
										</td>

										<td>
											<div style="margin-left:15px;" class="price">{{number_format($stavka->price, 0, ',', '.')}} rsd</div>
										</td>
										<td>
                                            <div class="qty">
                                                    <ul>
                                                            <li><span onclick="azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! $stavka->id!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').html()) - 1 )" class="qtminus"><i class="fas fa-minus"></i></span></li>
                                                            <li><span id="qttotal-stavka-{{$stavka->rowId}}" class="qttotal">{{$stavka->qty}}</span></li>
                                                            <li><span onclick="azurirajKolicinuUKorpi('{!! $stavka->rowId !!}', '{!! $stavka->id!!}', parseInt($('#qttotal-stavka-{{$stavka->rowId}}').html()) + 1 )" class="qtplus"><i class="fas fa-plus"></i></span></li>
                                                    </ul>
                                            </div>
										</td>
										<td>
                                            <div id="stavka-total-{{$stavka->rowId}}" style="margin-left:15px;" class="price">{{$stavka->total(0, ',', '.')}} rsd</div>
										</td>
									</tr>
									@endforeach

								</tbody>
							</table>

							<br/><br/>

							@if(count($stavkeVauceri) > 0)
								@include('include.korpaVauceri', ['stavkeVauceri', $stavkeVauceri])
							@endif
						</div>
					</div><!-- //. top content -->
					<div class="bottom-content"><!-- bottom content -->
						<div class="left-content-area">
								<div class="coupon-code-wrapper">
										<div class="form-element">
												<input id="kod" type="text" class="input-field" placeholder="Kod kupona">
										</div>
										<button onclick="primeniKupon()" type="button" class="submit-btn">Primeni kupon</button>
										<p id="kupon-text"></p>
								</div>
						</div>
						<div class="right-content-area">
								<div class="btn-wrapper">

                                    <a href="/naplati">	<button type="submit" class="boxed-btn"> Završi kupovinu </button> </a>

								</div>
								<div class="cart-total">
										<h3 class="title">Ukupan iznos porudžbine</h3>
										<ul class="cart-list">
											<li>Cena <span id="total-cena" class="right">{{number_format($total,0 , ',', '.')}} rsd</span></li>
											<li>Dostava <span class="right">280 rsd</span></li>
											<li class="total">Ukupno <span id="total-cena-dostava" class="right">
												<strong>
													{{number_format($total + 280,0 , ',', '.')}} rsd
												</strong>
											</span>
										</li>
										</ul>
								</div>
						</div>

					</div><!-- //. bottom content -->
				</div><!-- //. cart content inner -->
				@endif
			</div>
		</div>
	</div>
</div>

<!-- Cart Sectin End -->
@stop