@extends('layout')

@section('title')
Maderoterapija Lica Edukacija Srbija Beograd - Oklagije za maderoterapiju
@stop

@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Maderoterapija Lica
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/maderoterapija-lica">Maderoterapija Lica</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					Termini grupne obuke:
					
				</h2>
				<h5>
					Beograd: 25. maj 2019.godine
					<br>Termine za individualne obuke možete dogovoriti pozivom na broj: 062 455 200
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>Video</h5>
								<h5>Play</h5>
						</div>
					</div>
				</div>
				-->
			</div>
			<div class="col-lg-12">
				<h5>
					<br>
					
				</h5>
				
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->




<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							Edukujte se za terapeuta maderoterapije lica
						</h2>
					
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						Osim maderoterapije tela koja je postala apsolutno najpopularnija tehnika oslobađanja od celulita,  maderoterapija lica je takođe sve traženiji tretman u salonima lepote.Oni koji su probali  facijalnu maderoterapiju uverili su se da ona dovodi do poboljšanja tonusa kože lica i daje efekat podmlađivanja jer podstiče proizvodnju elastina i kolagena.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	Tematske jedinice koje obrađujemo:
	<br><br>
	</h2>
	<div class="row">
		<h5 style="color: white;">
			<strong>1 ČAS </strong>- teorijska predavanja –Koža, Poreklo i efekti maderoterapije, Oprema prostorije za maderoterapiju, Sredstva za maderoterapiju, Dejstvo maderoterapije, Doziranje maderoterapije, Indikacije i kontraindikacije za primenu maderoterapije. 
			<br>

			<strong>3 ČASA </strong>- praktična nastava –Tehnike facijalne maderoterapije
			<br>
			Nakon obuke polaznici polažu teorijski i praktični ispit i stiču sertifikat o stručnoj osposobljenosti za rad na poslovima terapeuta maderoterapije lica.
		</h5>
		
	</div>
</div>
</section>
<!-- Counter Area End -->

<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
						<h2>
							Trajanje edukacije:
						</h2>
					
					<h5>
						Ukupan fond časova edukacije je 4. Nastava se odvija tokom jednog dana sa početkom od 10h. U pauzi je obezbedjeno osveženje.
						<br><br>
					</h5>

					<img src="{{asset('img/sectionSeparator.png')}}" alt="">

					<h2>
						<br>
							Potrebna dokumentacija za upis:
						</h2>
					
					<h5>
						Prilikom upisa potrebno je da nam dostavite: fotokopiju prethodno stečene diplome redovnog obrazovanja i fotokopiju lične karte.
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2">
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>START</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-diamond"></i>
					</div>
					<div class="doller">
					<span>12.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Skripta</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong><BR><BR></strong>
							</li>
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="p_box">
					<div class="doller">
					<span>SMART</span>
					</div>
					<div class="title">
						<img class="img-fluid" src="{{asset('img/shape1.png')}}" alt="">
						<i class="pe-7s-magic-wand"></i>
					</div>
					<div class="doller">
					<span>19.000rsd</span>
					</div>
					<div class="list">
						<ul>
							<li>
								<strong>Edukacija</strong>
							</li>
							<li>
								<strong>Skripta</strong>
							</li>
							<li>
								<strong>Sertifikat</strong>
							</li>
							<li>
								<strong>5 drvenih maderoelemenata za rad</strong>
							</li>
							
						</ul>
					</div>
					<a class="phurchaseBtn" href="/kontakt">
						Zakažite termin	
					</a>
				</div>
			</div>
			
		</div>
		<div class="row justify-content-center">	
			<h5>
				<br>
				*Na individualnu obuku je potrebno dovesti osobu koja će biti model za vežbu.<br>
				Cena za individualnu edukaciju je 30% veća.
			</h5>
		</div>

		


	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong> <a href="tel:062/455200">062/455200</a> </strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



