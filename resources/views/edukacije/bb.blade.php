@extends('layout')

@section('title')
BB Glow Edukacija Srbija - Ampule Za BB Glow - 
@stop


@section('sekcije')
<!-- banner Area Start -->
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						BB Glow
					</h2>
					<div class="links">
						<ul>
							<li>
								<a href="/">Naslovna</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a href="/edukacija">Edukacija</a>
							</li>
							<li>
								<span>/</span>
							</li>
							<li>
								<a class="active" href="/edukacija/bb-glow">BB Glow</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->



<!-- Video Section Start -->
<section id="video" class="video">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="vImg">
					<img class="img-fluid" src="{{asset('img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg')}}" alt="">
				</div>
			</div>
			<div class="col-lg-6">
				<h2>
					BB Glow 
				</h2>
				<h5>
					Probajte da zamislite svoje lice koje izgleda besprekorno i blistavo, bez svakodnevnog šminkanja! Mislite da to nije moguće?
						Lepa vest je da ne morate da zamišljate jer je planetarno najpopularniji tretman BB Glow stigao u Srbiju! Dugo i pažljivo čuvana tajna savršenog tena žena iz Koreje je konačno otkrivena i dostupna damama u našoj zemlji. Istražite novi revolucionarni tretman kože sa visoko kvalitetnim proizvodima.<br>
						
						
				</h5>
				<!--
				<div class="videoContent">
					<div class="icon">
						<a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
							<i class="fas fa-play"></i>
						</a>
					</div>
					
					<div class="content d-flex">
						<div class="text align-self-center">
							<h5>intro</h5>
								<h5>Play Video</h5>
						</div>
					</div>
					
				</div>
				-->
			</div>
		</div>
	</div>
</section>
<!-- Video Section End -->


<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-10">
				<div class="sectionTheading">
					
						<h2>
							Besprekoran ten bez šminke?<br> Da, moguće je!
						</h2>
					
					<h2>
						Šta je to BB Glow?
					</h2>
					<img src="{{asset('img/sectionSeparator.png')}}" alt="">
					
					<h5>
						BB Glow je prijatan i bezbedan tretman kojim postižemo ujednečen ten, kao i poboljšanje tonusa kože lica. Najnoviji trend u tehnologiji za negu kože. Pruža trenutnu pokrivenost i dugotrajnu lepotu bez preterane stimulacije kože.  Ovim tretmanom postižemo prirodni izgled besprekorne i blistave kože lica, lagano posvetljujemo i toniziramo kožu, a sve to uz prikrivanje nedostataka kao što su flekice, pegice, crvenilo ili tamni podočnjaci.  Povrh svega navedenog, BB Glow ima i hidrirajući antiage efekat!
					</h5>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Counter Area Start -->
<section id="counter" class="counter">

<div class="container">
	<h2 style="color: white; text-align:center;">
	REZULTATI BB GLOW TRETMANA
	<br><br>
	</h2>
	<div class="row">
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">1</span>
				<h5 style="color: white;">Kože lica dobija trenutni sjaj , hidrirana je i jedra</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">2</span>
				<h5 style="color: white;">Ublaženi su podočnjaci, pege, fleke i ožiljci od akni</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">3</span>
				<h5 style="color: white;">Problem hiperpigmentacija i crvenila je rešen</h5>
			</div>
		</div>
		<div class=" col-md-6 col-lg-3">
			<div class="c-box">
				<span class="">4</span>
				<h5 style="color: white;">Sitne bore postaju nevidljive, BB Cream efekat</h5>
			</div>
		</div>
	</div>
</div>
</section>
<!-- Counter Area End -->

<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
	<div class="container">
			<div class="row justify-content-center">
					<div class="col-md-10 col-lg-8">
				<div class="sectionTheading">
					<h2>
					Cena edukacije 
					</h2>
					<img src="{{asset('img/sectionSeparatorw.png')}}" alt="">
					<h5>
							Izaberite svoj promo paket uz edukaciju za BB Glow tretman:
					</h5>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>basic</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="img/shape1.png" alt="">
                        <i class="pe-7s-leaf"></i>
                    </div>
                    <div class="doller">
                    <span>190 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Workshop</strong>
                            </li>
                            <li>
                                <strong>Potvrda o prisustvu </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Specijalni popust za nabavku materijala za rad na dan workshopa<br><br></strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                            
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>start</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="img/shape1.png" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                    <span>590 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Skripta </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Mini starter set</strong>
                            </li>
                            <li>
                                <strong>Trajni popust od 10% na proizvode</strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>smart</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="img/shape1.png" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                    <span>690 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Skripta </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Starter set</strong>
                            </li>
                            <li>
                                <strong>Dermapen</strong>
                            </li>
                            <li>
                                <strong>Trajni popust od 10% na proizvode</strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            
        </div>
	</div>
</section> 
<!-- Pricing Plan End -->
<section id="serviceSection" class="serviceSection">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-12 col-lg-12">
				<div class="sectionTheading">
					<!--
					<img src="{{asset('/img/in.png')}}" alt="">
					-->
						<h4>
							Prijavite se odmah na našu edukaciju koja je apsolutno jedinstvena u Srbiji po kvalitetu rada sa polaznicima i nivou stečenog znanja na broj: <strong><a href="tel:062/455200">062/455200</a></strong> ili putem maila: <strong>info@essenceofbeauty.rs</strong>
						</h4>
				

				</div>
			</div>
		</div>
	</div>
</section>
@stop



