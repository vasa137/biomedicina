@extends('layout')

@section('title')
{{$proizvod->naziv}} - 

@foreach($proizvod->kategorije as $kategorija)
	{{$kategorija->naziv}}
	@if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
		, 
	@endif
@endforeach

@stop

@section('scriptsTop')
	<link href="{{asset('css/slick-lightbox.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/popupDialog.css')}}"/>
@endsection
@section('scriptsBottom')
	<script src="{{asset('js/slick-lightbox.js')}}"></script>
    <script src="{{asset('js/klijentKorpa.js')}}"></script>
    <script src="{{asset('js/popupDialog.js')}}"></script>
    <script src="{{asset('js/klijentProizvod.js')}}"></script>
	<script>
		inicijalizujSlike();
	</script>
@endsection

@section('sekcije')
@include('include.popupDialog', ['poruka' => 'Uspešno ste dodali proizvod u korpu. Želite li da pregledate korpu?', 'linkUspesno' => '/korpa' ,'yesButtonText' => 'Da' ,'noButtonText' => 'Ne'])
<section id="spabreadcrumb" class="spabreadcrumb">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						{{$proizvod->naziv}}
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Naslovna</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="#">{{$proizvod->naziv}}</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->

<!-- Product details Section Srart -->

<!-- product details content area  start -->
<div class="product-details-content-area">
	<div class="container">
		<div class="row">
			<div class="col-xl-6">
				<div class="left-content-area">
					<div class="product-details-slider slider-for" id="product-details-slider" data-slider-id="1">
						@if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
							<div class="single-product-thumb">
								<a href="/images/proizvodi/{{$proizvod->id}}/glavna/{{$proizvod->nazivGlavneSlike}}.jpg"><img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}"></a>
							</div>
						@endif
						@foreach($proizvod->sveSlike as $slika)
							<div class="single-product-thumb">
								<a href="/images/proizvodi/{{$proizvod->id}}/sveSlike/{{$slika}}"><img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" alt="{{$proizvod->naziv}}"></a>
							</div>
						@endforeach
					</div>
					<ul class="product-deails-thumb" data-slider-id="1">
						@if(File::exists(public_path('/images/proizvodi/'.$proizvod->id . '/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')))
							<li class="owl-thumb-item">
								<img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/glavna/' . $proizvod->nazivGlavneSlike . '.jpg')}}" alt="{{$proizvod->nazivGlavneSlike}}">
							</li>
						@endif
						@foreach($proizvod->sveSlike as $slika)
							<li class="owl-thumb-item">
								<img class="img-fluid" src="{{asset('images/proizvodi/'. $proizvod->id .'/sveSlike/' . $slika)}}" alt="{{$proizvod->naziv}}">
							</li>
						@endforeach
					</ul>
				</div><!-- //.left content area -->
			</div>
			<div class="col-xl-6">
				<div class="right-content-area">
					<div class="bottom-content">

						@if(!empty($proizvod->kategorije))

							@foreach($proizvod->kategorije as $kategorija)
								<span class="cat">{{$kategorija->naziv}}</span>
								@if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
									|
								@endif
							@endforeach

						@endif
						<h3 class="title">{{$proizvod->naziv}}</h3>
						<div class="price-area">
							<div class="left">
								@if($proizvod->na_popustu)
									<span class="sprice">{{number_format($proizvod->cena_popust, 0, ',', '.')}} RSD</span>
									<span class="dprice"><del>{{number_format($proizvod->cena, 0, ',', '.')}} RSD</del></span>
								@else
									<span class="sprice">{{number_format($proizvod->cena, 0, ',', '.')}} RSD</span>
								@endif
							</div>
							
						</div>
						<ul class="product-spec">

							<li>Šifra: <span class="right">{{$proizvod->sifra}}</span></li>
							
							@if($proizvod->id_brend != null) <li>Brend: <span class="right">{{$proizvod->brend->naziv}} </span></li> @endif
							@if(!empty($proizvod->kategorije))
								<li>Kategorija:
									<span class="right base-color">
										@foreach($proizvod->kategorije as $kategorija)
												{{$kategorija->naziv}}
												@if($kategorija != $proizvod->kategorije[count($proizvod->kategorije) - 1])
													,
												@endif
										@endforeach
									</span>
								</li>
							@endif
							<!--
							<li>Zapremina: <span class="right">30 ml</span></li>
							<li>Količina: <span class="right">10 komada u kutiji</span></li>
							-->
							@if($proizvod->ima_specifikacije)
								<?php for($i = 0; $i < count($proizvod->specifikacije); $i++) { ?>
									<li>{{$proizvod->specifikacije[$i]->naziv}}: <span class="right">{{$proizvod->specifikacije_tekst[$i]}}</span></li>
								<?php } ?>
							@endif
						</ul>
							<br/>	<br/>
						<div class="pdescription">
							<!--
							<h4 class="title">Opis</h4>
						-->
							<p>{{$proizvod->opis}}</p>

						</div>
						<div class="paction">
							<div class="qty">
								<ul>
									<li><span class="qtminus"><i class="fas fa-minus"></i></span></li>
									<li><span id="kolicina" class="qttotal">1</span></li>
									<li><span class="qtplus"><i class="fas fa-plus"></i></span></li>
								</ul>
							</div>
							<div class="btn-wrapper">
								<a href="javascript:dodajUKorpu('{!! $proizvod->id !!}', $('#kolicina').html())" class="boxed-btn">Dodaj u korpu</a>
							</div>
						</div>
					</div>
				</div><!-- //. right content area -->
			</div>
		</div>

		<!--
		<div class="row">
			<div class="col-lg-12">
				<div class="product-details-area">
					<div class="product-details-tab-nav">
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="item-review-tab" data-toggle="tab" href="#item_review" role="tab" aria-controls="item_review"
								 aria-selected="true">Opis proizvoda</a>
							</li>

							
							<li class="nav-item">
								<a class="nav-link" id="descr-tab" data-toggle="tab" href="#descr" role="tab" aria-controls="descr"
								 aria-selected="false">Specifikacija</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="method-tab" data-toggle="tab" href="#method" role="tab" aria-controls="method"
								 aria-selected="false">Neki info...</a>
							</li>

							
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane fade show active" id="item_review" role="tabpanel" aria-labelledby="item-review-tab">
							<div class="descr-tab-content">
								
								<p>
								{{$proizvod->opis}}
								</p>
							</div>
						</div>
						<div class="tab-pane fade" id="descr" role="tabpanel" aria-labelledby="descr-tab">
							
							<div class="item_review_content">
								<h4 class="title">Specifikacija</h4>
								<ul class="product-specification">
									<li>
										<div class="single-spec">
											<span class="heading">Dimenzije</span>
											<span class="details">21.9 x 31.4 x 1.8 cm</span>
										</div>
									</li>
									<li>
										<div class="single-spec">
											<span class="heading">Težina</span>
											<span class="details">0.3 Kg</span>
										</div>
									</li>
									<li>
										<div class="single-spec">
											<span class="heading">Namena</span>
											<span class="details">Maderoterapija</span>
										</div>
									</li>
									<li>
										<div class="single-spec">
											<span class="heading">Neki info</span>
											<span class="details">Maderoterapija</span>
										</div>
									</li>
									
								</ul>
							</div>
						</div>
						<div class="tab-pane fade" id="method" role="tabpanel" aria-labelledby="method-tab">
							<div class="more-feature-content">
								<h4 class="title">Dodatni tab</h4>
								<div class="feature-list-wrapper">
									<div class="row">
										<div class="col-lg-3 col-md-6">
											<ul class="features-list">
												<li><i class="fas fa-check"></i> 24/7 Online Support</li>
												<li><i class="fas fa-check"></i> 24/7 Online Support</li>
												<li><i class="fas fa-check"></i> 24/7 Online Support</li>
												<li><i class="fas fa-check"></i> 24/7 Online Support</li>
											</ul>
										</div>
										<div class="col-lg-3 col-md-6">
											<ul class="features-list">
												<li><i class="fas fa-check"></i> Unlimited Features</li>
												<li><i class="fas fa-check"></i> Unlimited Features</li>
												<li><i class="fas fa-check"></i> Unlimited Features</li>
												<li><i class="fas fa-check"></i> Unlimited Features</li>
											</ul>
										</div>
										<div class="col-lg-3 col-md-6">
											<ul class="features-list">
												<li><i class="fas fa-check"></i> 100% Pure cotton</li>
												<li><i class="fas fa-check"></i> 100% Pure cotton</li>
												<li><i class="fas fa-check"></i> 100% Pure cotton</li>
												<li><i class="fas fa-check"></i> 100% Pure cotton</li>
											</ul>
										</div>
										<div class="col-lg-3 col-md-6">
											<ul class="features-list">
												<li><i class="fas fa-check"></i> Simple and easy</li>
												<li><i class="fas fa-check"></i> Simple and easy</li>
												<li><i class="fas fa-check"></i> Simple and easy</li>
												<li><i class="fas fa-check"></i> Simple and easy</li>
											</ul>
										</div>
									</div>
								</div>
								<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and
									a search for 'lorem ipsum' will uncove
									many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident,
									sometimes on purpose (injected humour
									and the like) Many desktop publishing packages and web page editors now use Lorem Ipsum as their default
									model text, and a search for 'lorem ipsum
									will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes
									by accident, sometimes on purpose
									(injected humour and the like)..</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		-->
	</div>
</div>
<!-- product details Section End -->
@stop