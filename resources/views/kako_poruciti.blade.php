@extends('layout')

@section('title')
Kako poručiti? - 
@stop

@section('sekcije')
<section id="spabreadcrumb" class="spabreadcrumb extrapadding">
	<div class="bcoverlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="Content">
					<h2>
						Kako poručiti?
					</h2>
					<div class="links">
					<ul>
						<li>
							<a href="/">Naslovna</a>
						</li>
						<li>
							<span>/</span>
						</li>
						<li>
							<a class="active" href="/kako-poruciti">Kako poručiti?</a>
						</li>
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Welcome Area End -->
 <!-- FAQ Section Start-->
 <section class="faqSection">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="faq-content">
					<div class="faq-accordian">
						<div class="panel-group accordion" id="accordionId">
											
							<div class="panel panel-default">
								<div class="panel-heading" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
											Poručivanje</a>
									</h4>
								</div>
								<div id="collapse1" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionId">
									<div class="panel-body">Naručivanje možete izvrštiti i bez registracije, ali Vam savetujemo da se registrujete kako bi ste skratili postupak svakog narednog naručivanja - podaci za isporuku će biti automatski popunjeni. Takođe registracijom na sajt beleže se sve Vaše porudžbine pa ćete u određenom trenutku dobiti razne vrste pogodnosti, a takođe ćete uvek moći da pregledate iste.</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
											Registracija i prijavljivanje</a>
									</h4>
								</div>
								<div id="collapse2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionId">
									<div class="panel-body">Ukoliko već imate nalog, klikom na ”Moj nalog” možete se prijaviti. Ukoliko se niste ranije registrovali to možete uraditi klikom na link ”Registruj se”. Registracija je veoma jednostavna. Sve što je potrebno da uradite je da popunite nekoliko potrebnih polja, nakon čega će Vaš nalog biti aktivan.</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
											Prodavnica</a>
									</h4>
								</div>
								<div id="collapse3" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionId">
									<div class="panel-body">Sve proizvode možete pogledati u odeljku ”Prodavnica”. Ukoliko želite možete primeniti neki od filtera i izabrati proizvod koji Vam najviše odgovara. Klikom na proizvod dobićete osnovne informacije o proizvodu. Za sve dodatne informacije o proizvodima možete nas kontaktirati. Klikom na ”Dodaj u korpu” proizvod će biti u Vašoj korpi.</div>
								</div>
							</div>		
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="faq-content">
					<div class="faq-accordian">
						<div class="panel-group accordion" id="accordionId2">	
							<div class="panel panel-default">
								<div class="panel-heading" id="headingseven">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
											Korpa i naplaćivanje </a>
									</h4>
								</div>
								<div id="collapse7" class="collapse" aria-labelledby="headingseven" data-parent="#accordionId2">
									<div class="panel-body">Kada ste odobrali željene proizvode, kliknite na korpu. Tu se nalaze svi proizvodi koje ste izabrali. Ukoliko imate kupon za popust tu ga možete primeniti. Nakon toga kliknite na naplatu. Na ovoj stranici se unose podaci za plaćanje i isporuku. Molimo Vas da pre poručivanja proverite podatke.</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingseven">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
											Korpa i naplaćivanje </a>
									</h4>
								</div>
								<div id="collapse8" class="collapse" aria-labelledby="headingseven" data-parent="#accordionId2">
									<div class="panel-body">Pošiljku šaljemo putem kurirske službe City Express, a plaćanje se vrši pouzećem. Naknadu za dostavu, u iznosu od 280 rsd, plaćate pri preuzimanju pošiljke.</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading" id="headingseven">
									<h4 class="panel-title">
										<a class="collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
											Rok isporuke </a>
									</h4>
								</div>
								<div id="collapse9" class="collapse" aria-labelledby="headingseven" data-parent="#accordionId2">
									<div class="panel-body">Robu šaljemo svakog radnog dana do 15 časova. Rok isporuke je 2 do 3 radna dana.</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- FAQ Section Start-->



@stop