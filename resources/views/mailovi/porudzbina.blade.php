<!DOCTYPE html >
<html>

<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
<link rel="stylesheet" type="text/css" href="{{asset('css/mailTemplate.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">

</head>
 
<body style="background-color: #30b1bf;">

<!-- HEADER -->
<div class="container" style="padding-top:70px; padding-bottom: 70px;">
	
	<table class="head-wrap" bgcolor="#bb6dac">
		<tr>
			<td></td>
			<td class="header container">
				
					<div class="content">
						<table >
							<tr>
								<td>
									
								</td>
								<td align="right">
									<h3 style="color: white !important;" class="collapse">Order Confirmed</h3>	
								</td>
							</tr>
						</table>
					</div>
					
			</td>
			<td></td>
		</tr>
	</table><!-- /HEADER -->


	<!-- BODY -->
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container" bgcolor="#FFFFFF">

				<div class="content">
				<table>
					<tr>
						<td>
							
							
							<!-- You may like to include a Hero Image -->
							<p><img src="{{asset('img/logo.png')}}" style="width: 120px;height:auto;" /></p>
							<!-- /Hero Image -->
							
							<br/>
							
							<h4>Potvrda porudžbine #{{$porudzbina->id}} <small> {{ date('d.m.Y - H:i', strtotime($vreme)) }}</small></h4>
							<p>Poštovani, Vaša porudžbina je uspešno kreirana i biće isporučena u roku od 2 do 3 radna dana. Ukoliko želite da izmenite porudžbinu molimo Vas da nas kontaktirate na 069-455-90-99</p>
								
							<br/>


							@foreach($stavke as $s)
							<!-- Line Items -->
							<div class="products">
								<img src="{{asset('images/proizvodi/' . $s->id_proizvod . '/glavna/' . $glavneSlike[$s->id_proizvod]. '.jpg')}}" style="width:140px; height:auto;" />
								<span> {{$s->kolicina}}x {{$proizvodi[$s->id]->naziv}}
									@if($s->id_kupon != null)
									 (kupon: {{$kuponi[$s->id_kupon]->kod}} )
									@endif

									@if($s->cena_popust < $s->cena)
										<del>{{number_format($s->cena, 0, ',', '.')}}</del>
										{{number_format($s->cena_popust, 0, ',', '.')}}
									@else
										{{number_format($s->cena_popust, 0, ',', '.')}}
									@endif


								= {{number_format($s->ukupno_popust, 0, ',', '.')}} rsd
								</span>
							</div>
							<hr>
							<div style="clear:both;"></div>
							@endforeach
							<!-- /Line Items -->
							
							<br/>
							<br/>
							@if($porudzbina->ima_vaucere)
								@foreach($vauceri as $vaucer)
									<h4>Vaučer {{$vaucer->kod}}: -{{number_format($vaucer->iznos, 0, ',', '.')}} rsd</h4>
								@endforeach
							@endif
							<!-- Totals -->
							<h4><b>Cena:</b> {{number_format($porudzbina->iznos, 0, ',', '.')}} rsd</h4>
							
							<!-- Totals -->
							<table class="columns" width="100%">
								<tr>
									<td>
										
										
											<!--- column 1 -->
											<table align="left" class="column">
												<tr>
													<td>
														<p><b>Dostava:</b> 280 rsd</p>
													</td>
												</tr>
											</table>
											<!-- /column 1 -->
										
										
										<!-- Discounts -->
										
											<!--- column 2 -->
											<table align="left" class="column">
												<tr>
													<td>
														<p><b>Popust:</b> {{number_format($porudzbina->iznos-$porudzbina->iznos_popust, 0, ',', '.')}} rsd </p>
													</td>
												</tr>
											</table>
											<!-- /column 2 -->
										
										<!-- /Discounts -->
										
										<span class="clear"></span>	
										
									</td>
								</tr>
							</table>
							<!-- /Totals -->
							
							
							<br/>
							<br/>
							
							<h4><b>Ukupno: {{number_format($porudzbina->iznos_popust+280, 0, ',', '.')}} rsd</b></h4>
							<!-- /Totals -->
							
							<br/>

							<!-- address detals -->
							<table class="columns" width="100%">
								<tr>
									<td>
										
											<!--- column 1 -->
											<table align="left" class="column">
												<tr>
													<td>				
														<h5 class="">Podaci za dostavu</h5>
														<p class="">
															{{$porudzbina->kupac}}<br/>
															{{$porudzbina->adresa}}<br/>
															{{$porudzbina->zip}}, {{$porudzbina->grad}}<br/>
															{{$porudzbina->telefon}}<br/>
															{{$porudzbina->email}}<br>
															<br>
															<strong>Napomena:</strong><br>
															{{$porudzbina->napomena}}<br>
														</p>
													</td>
												</tr>
											</table>
										<!-- /column 1 -->
										
										
										
										
										<span class="clear"></span>	
										
									</td>
								</tr>
							</table>
							<!-- /address details -->
							
							<br/>
							
							<p style="text-align:center;">
								<a class="btn" href="{{url('/nalog')}}">Pogledajte Vaše prethodne posiljke &raquo;</a>
							</p>
							
							<br/>

						
						</td>
					</tr>
				</table>
				</div>
										
			</td>
			<td></td>
		</tr>
	</table>
	<!-- /BODY -->

	<!-- FOOTER -->
	<table class="footer-wrap" bgcolor="#222222">
		<tr>
			<td></td>
			<td class="container">
				
					<!-- content -->
					<div class="content">
					<table>
					<tr>
						<td align="center">
							<a href="/" target="_blank"><img src="{{asset('img/logo-footer.png')}}" style=" width:140px; height:auto" alt="/" /></a>
							<br/><br/>
							<p><strong><a href="mailto:info@essenceofbeauty.rs" style="color:#bb6dac;">info@essenceofbeauty.rs</a></strong></p>
						</td>
					</tr>
				</table>
					</div><!-- /content -->
					
			</td>
			<td></td>
		</tr>
	</table><!-- /FOOTER -->
</div>

</body>
</html>