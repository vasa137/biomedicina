@extends('layout')

@section('scriptsTop')
    <link rel="stylesheet" href="{{asset('css/sliderPocetna.css')}}"/>
@endsection

@section('scriptsBottom')
    <script src="{{asset('js/jssor.slider-27.5.0.min.js')}}"></script>
    <script src="{{asset('js/sliderPocetna.js')}}"></script>
    <script>jssor_1_slider_init();</script>
@endsection

@section('sekcije')

    <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:440px;overflow:hidden;visibility:hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" class="jssorl-004-double-tail-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
            <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="img/double-tail-spin.svg" />
        </div>
        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:500px;overflow:hidden;">
            <div>
                <!-- #endregion Jssor Slider End -->
                <section id="banner" class="banner" style=" background: url(/img/slider/banner1.jpg) no-repeat;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                        Dobrodošli na sajt kompanije
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <a class="link1" href="/prodavnica">
                                            Prodavnica
                                        </a>
                                        <a class="link2" href="/edukacija">
                                            Edukacija
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </div>

            <div>
                <section id="banner" class="banner" style="background: url(/img/slider/banner2.jpg) no-repeat center;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                       Dobrodošli na sajt kompanije
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <a class="link1" href="/prodavnica">
                                            Prodavnica
                                        </a>
                                        <a class="link2" href="/edukacija">
                                            Edukacija
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div>
                <section id="banner" class="banner" style="background: url(/img/slider/banner3.jpg) no-repeat center;">
                    <div class="bannerOverlay"></div>
                    <div class="container" style="position:absolute;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="content">
                                    <p class="tagline">
                                        Dobrodošli na sajt kompanije
                                    </p>
                                    <h1>
                                        Essence Of Beauty
                                    </h1>
                                    <div class="links">
                                        <a class="link1" href="/prodavnica">
                                            Prodavnica
                                        </a>
                                        <a class="link2" href="/edukacija">
                                            Edukacija
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="1" data-scale="0.5" data-scale-bottom="0.75">
            <div data-u="prototype" class="i" style="width:16px;height:16px;">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                </svg>
            </div>
        </div>
        <!-- Arrow Navigator -->
        <div data-u="arrowleft" class="jssora051" style="width:55px;height:55px;top:0px;left:25px;" data-autocenter="2" data-scale="0.75" data-scale-left="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="11040,1920 4960,8000 11040,14080 "></polyline>
            </svg>
        </div>
        <div data-u="arrowright" class="jssora051" style="width:55px;height:55px;top:0px;right:25px;" data-autocenter="2" data-scale="0.75" data-scale-right="0.75">
            <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                <polyline class="a" points="4960,1920 11040,8000 4960,14080 "></polyline>
            </svg>
        </div>
    </div>


<!-- 
<section id="services" class="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="sBox">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon1.png" alt="">
                    </div>
                    <h3>
                        BESPLATNA DOSTAVA
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox box2">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon2.png" alt="">
                    </div>
                    <h3>
                        EDUKACIJA
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon3.png" alt="">
                    </div>
                    <h3>
                        INFO 3
                    </h3>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sBox box2">
                    <div class="img">
                        <img class="align-self-center" src="img/icon/icon4.png" alt="">
                    </div>
                    <h3>
                        INFO 4
                    </h3>
                </div>
            </div>
        </div>
    </div>
</section>
 -->

<section id="giftCard" class="giftCard">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        Izdvajamo iz ponude
                        <br><br>
                    </h2>
                    <!--
                    <img src="img/sectionSeparator.png" alt="">
                    
                    <p>
                        
                    </p>
                -->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/oklagijice.png" alt="">
                    </div>
                    <div class="cardContent">
                        <h3>
                            Profesionalni set za maderoterapiju lica -30%
                        </h3>
                        <p>
                            Osim maderoterapije tela koja je postala najpopularnija tehnika oslobađanja od celulita, maderoterapija lica je takođe sve traženiji tretman u salonima lepote.
                        </p>
                        <a href="/proizvod/Profesionalni-set-za-maderoterapiju-lica/10">
                            Pogledajte ponudu
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/ampule-za-bb-glow-tretman.jpg" alt="">
                    </div>
                    <div class="cardContent">
                        <h3>
                            BB Glow ampule<br><br>
                        </h3>
                        <p>
                            BB Glow je prijatan i bezbedan tretman kojim postižemo ujednečen ten, kao i poboljšanje tonusa kože lica.
                            <br><br>
                        </p>
                        <a href="/prodavnica">
                            Pogledajte ponudu
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="mycard">
                    <div class="cardImage">
                        <img class="img-fluid" src="img/oklagije-za-maderoterapiju-tela.jpg" alt="">
                    </div>
                    <div class="cardContent">
                        <h3>
                            Oklagije za maderoterapiju tela -10%
                        </h3>
                        <p>
                            Čuli ste da je anticelulit masaža oklagijom najdelotvorniji i najprirodniji način za eliminaciju celulita, a takodje i najtraženija usluga u kozmetičkim salonima? 
                        </p>
                        <a href="/prodavnica">
                            Pogledajte ponudu
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Gifts & Cards Section End -->


<!-- News Feeds Area CSS start -->
<section id="news" class="news">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        Edukacije
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                        Naredni termini naših najtraženijih edukacija su:
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-lica-kurs-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija-lica">
                            <h3>
                               Maderoterapija lica 25.5.2019.
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/maderoterapija-tela-obuka-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/maderoterapija">
                            <h3>
                               Maderoterapija tela 26.5.2019.
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="blog-box">
                    <div class="topImg">
                        <a href="/edukacija/bb-glow">
                            <img class="img-fluid" src="img/edukacije-maderoterapija-tela-lica-bb-glow-srbija/bb-glow-srbija-edukacija-naslovna.jpg" alt="">
                        </a>
                    </div>
                    <div class="text">
                        <a href="/edukacija/bb-glow">
                            <h3>
                               BB Glow<br>2.6.2019.
                            </h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- News Feeds Area CSS End -->

<!-- Video Section Start 
<section id="video" class="video">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="vImg">
                    <img class="img-fluid" src="img/videoSection.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <h2>
                    Pzicija za video
                </h2>
                <p>
                    Ovde bi bio video na kome se može prikazati obuka ili neka od usluga. Takođe možemo napraviti doodly video po potrebi gde mozemo raditi npr sta se dobija nekom od obuka sa štikliranim pozicijama...
                </p>
                <div class="videoContent">
                    <div class="icon">
                        <a class="venobox" data-vbtype="video" data-autoplay="true" href="http://vimeo.com/75976293">
                            <i class="fas fa-play"></i>
                        </a>
                    </div>
                    <div class="content d-flex">
                        <div class="text align-self-center">
                            <h5>obuka - Maderoterapija tela</h5>
                                <p>Pogledajte video</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
Video Section End -->


<!-- Appoinment Section Start -->
<section id="appoinmentSection" class="appoinmentSection">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="">
                    <div style="text-align: center; color: white;">
                        <h2>Za naše edukacije traži se mesto više! Zbog kvaliteta edukacije rad je isključivo u malim grupama!<br><br></h2>

                        <a href="tel:+381 62 455 200">
                            <h2>
                                +381 62 455 200
                            </h2>
                            
                        </a>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Appoinment Section End -->



<!-- Counter Area Start 
<section id="counter" class="counter">
    <div class="container">
        <div class="row">
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-diamond"></i>
                    <span class="count">1000</span><sup>+</sup>
                    <h3>Uspešnih obuka</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-leaf"></i>
                    <span class="count">9997</span><sup>+</sup>
                    <h3>Zadovoljnih klijenata</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-magic-wand"></i>
                    <span class="count">8656</span><sup>+</sup>
                    <h3>Proizvedenih oklagija</h3>
                </div>
            </div>
            <div class=" col-md-6 col-lg-3">
                <div class="c-box">
                    <i class="pe-7s-coffee"></i>
                    <span class="count">1000</span><sup>+</sup>
                    <h3>Neki info</h3>
                </div>
            </div>
        </div>
    </div>
</section>
 Counter Area End -->





<!-- Pricing Plan Start -->
<section id="pricePlan" class="pricePlan">
    <div class="container">
            <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                <div class="sectionTheading">
                    <h2>
                    BB GLOW edukacija 
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                            Izaberite svoj PROMO paket edukacije za BB Glow tretman:
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>basic</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="img/shape1.png" alt="">
                        <i class="pe-7s-leaf"></i>
                    </div>
                    <div class="doller">
                    <span>190 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Workshop</strong>
                            </li>
                            <li>
                                <strong>Potvrda o prisustvu </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Specijalni popust za nabavku materijala za rad na dan workshopa<br><br></strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                            
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>start</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="img/shape1.png" alt="">
                        <i class="pe-7s-diamond"></i>
                    </div>
                    <div class="doller">
                    <span>590 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Skripta </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Mini starter set</strong>
                            </li>
                            <li>
                                <strong>Trajni popust od 10% na proizvode</strong>
                            </li>
                            <li>
                                <strong><br></strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="p_box">
                    <div class="doller">
                    <span>smart</span>
                    </div>
                    <div class="title">
                        <img class="img-fluid" src="img/shape1.png" alt="">
                        <i class="pe-7s-magic-wand"></i>
                    </div>
                    <div class="doller">
                    <span>690 €</span>
                    </div>

                    <div class="list">
                        <ul>
                            <li>
                                <strong>Edukacija</strong>
                            </li>
                            <li>
                                <strong>Skripta </strong>
                            </li>
                            <li>
                                <strong>Sertifikat</strong>
                            </li>
                            <li>
                                <strong>Starter set</strong>
                            </li>
                            <li>
                                <strong>Dermapen</strong>
                            </li>
                            <li>
                                <strong>Trajni popust od 10% na proizvode</strong>
                            </li>
                        </ul>
                    </div>
                    <a class="phurchaseBtn" href="/kontakt">
                        Zakažite termin 
                    </a>
                </div>
            </div>
            
        </div>
    </div>
</section> 
<!-- Pricing Plan End -->


<!-- Opening Time  Section Start -->
<section id="openingTime" class="openingTime">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-8">
                <div class="openingLeft">
                    <div class="left">
                        <img class="img-fluid" src="img/openingTime.jpg" alt="">
                    </div>
                    <div class="right">
                        <h2>
                            BB GLOW
                            MADERO
                            ANTICELULIT
                            MASAŽA
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-4">
                <div class="openingChart">
                    <div class="title">
                        <h2>
                            Radno vreme
                        </h2>
                    </div>
                    <div class="content">
                        <p>
                           Kozmetički salon Essence of Beauty nalazi se u Bulevaru Zorana Đinđića 6. Odiše toplinom i u njemu se klijenti tokom tretmana osećaju prijatno i relaksirano, a ljubaznost i profesionalnost osoblja je ključ našeg uspeha.
                        </p>
                        <ul class="time">
                            <li>
                                Pon-Pet: 08 – 20
                            </li>
                            <li>
                                Subota: 08 – 17
                            </li>
                            <li>
                                Nedelja: Ne radimo
                            </li>
                        </ul>
                        <a class="link" href="/kontakt">
                            Kontakt
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Opening Time  Section End -->


<!-- Team Section CSS Start -->
<!--
<section id="team" class="team">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10 col-lg-9">
                <div class="sectionTheading">
                    <h2>
                        Zadovoljni klijenti
                    </h2>
                    <img src="img/sectionSeparatorw.png" alt="">
                    <p>
                        When an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        It has survived not only five centuries took a galley of type and scrambled
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/team/team1.png" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Protik
                            </h4>
                            <h6>
                                Beauty Expart
                            </h6>
                            <p>
                                When an unknown printer took a
                                galley of type and scrambled it to
                                make a type specimen book.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/team/team2.png" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Shuvo
                            </h4>
                            <h6>
                                Beauty Expart
                            </h6>
                            <p>
                                When an unknown printer took a
                                galley of type and scrambled it to
                                make a type specimen book.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/team/team3.png" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Marija
                            </h4>
                            <h6>
                                BB Glow edukacija
                            </h6>
                            <p>
                                Prezadovolja sam obukom, veoma brzo sam stekla prakticno znanje i vestine koje sam odmah primenila u svom poslu...
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="t-box">
                    <div class="left">
                        <div class="img">
                            <img class=" img-fluid" src="img/team/team4.png" alt="">
                        </div>
                    </div>
                    <div class="right">
                        <div class="content">
                            <h4>
                                Mahan
                            </h4>
                            <h6>
                                Beauty Expart
                            </h6>
                            <p>
                                When an unknown printer took a
                                galley of type and scrambled it to
                                make a type specimen book.
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
-->
<!-- Team Section CSS End -->
@stop