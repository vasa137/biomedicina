<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//---------KORISNICI RUTE POCETAK----------------

Route::get('/', function () {
    return view('welcome');
});

Route::get('/prodavnica', 'klijentProizvodiController@prodavnica');

Route::get('/proizvod/{link}/{id}', 'klijentProizvodiController@proizvod');

Route::get('/kontakt', function () {
    return view('kontakt');
});

Route::post('/kontaktiraj', 'klijentKorisnikController@kontaktiraj');

Route::get('/blog','klijentBlogController@blog');

Route::get('/clanak/{link}/{id}','klijentBlogController@clanak');


/*-------------NOVE STRANE--------------*/
Route::get('/nalog22', function () {
    return view('nalog');
});

Route::get('/porudzbina-uspesna', function () {
    return view('uspesna_porudzbina');
});

Route::get('/kontakt-uspesan', function () {
    return view('uspesan_kontakt');
});



Route::get('/greska', function () {   /*OVO TREBA UMESTO ONE PODRAZUMEVANE 404 STRANE I SVIH OSTALIH ZA GRESKU*/
    return view('greska');
});

/*-------------NOVE STRANE--------------*/

Route::get('/nalog', 'klijentKorisnikController@nalog');

Route::get('/kako-poruciti', function () {
    return view('kako_poruciti');
});

Route::get('/uslovi-kupovine', function () {
    return view('uslovi');
});


//-----------EDUKACIJE-POCETAK-------------
Route::get('/edukacija', function () {
    return view('edukacija');
});

Route::get('/edukacija/maderoterapija', function () {
    return view('edukacije.maderoterapija');
});

Route::get('/edukacija/bb-glow', function () {
    return view('edukacije.bb');
});

Route::get('/edukacija/maderoterapija-lica', function () {
    return view('edukacije.maderoterapija_lica');
});

Route::get('/edukacija/relaks-masaza', function () {
    return view('edukacije.relaks');
});

Route::get('/edukacija/anticelulit-masaza', function () {
    return view('edukacije.anticelulit');
});

//-----------EDUKACIIJE-KRAJ------------

//-----------SALON-POCETAK-------------
Route::get('/salon', function () {
    return view('salon');
});

Route::get('/salon/tretmani-lica', function () {
    return view('salon.lice');
});

Route::get('/salon/epilacija', function () {
    return view('salon.epilacija');
});

Route::get('/salon/tretmani-tela', function () {
    return view('salon.telo');
});

Route::get('/salon/kozmeticke-usluge', function () {
    return view('salon.kozmeticke');
});

Route::get('/salon/cenovnik', function () {
    return view('salon.cenovnik');
});

Route::get('/salon/promocije', function () {
    return view('salon.promocije');
});

Route::get('/salon/bb-glow', function () {
    return view('salon.bb_glow');
});

Route::get('/salon/karijera', function () {
    return view('salon.karijera');
});


//-----------SALON-KRAJ------------


//---------KORISNICI RUTE KRAJ----------------



Route::get('/korpa', 'klijentKorpaController@pregled_korpe');


Route::get('/naplati', 'klijentKorpaController@naplati');

Route::post('/dodaj_u_korpu', 'klijentKorpaController@dodaj_u_korpu');
Route::post('/azuriraj_kolicinu_u_korpi', 'klijentKorpaController@azuriraj_kolicinu_u_korpi');
Route::post('/obrisi_iz_korpe', 'klijentKorpaController@obrisi_iz_korpe');
Route::post('/primeni_kupon_vaucer', 'klijentKorpaController@primeni_kupon_vaucer');
Route::post('/ukloni_vaucer_iz_korpe', 'klijentKorpaController@ukloni_vaucer_iz_korpe');
Route::post('/sacuvaj_porudzbinu', 'klijentKorpaController@sacuvaj_porudzbinu');

Route::get('/proveriRezervacije', 'klijentKorisnikController@proveri_rezervacije');

Route::get('/rezervacija',  'klijentKorisnikController@rezervacija');
Route::post('/rezervisi', 'klijentKorisnikController@rezervisi');

Route::get('/rezervacija-uspesna', function () {
    return view('rezervacijaUspesna');
});

Route::middleware(['auth'])->group(function () {
    Route::post('/promeni_licne_podatke', 'klijentKorisnikController@promeni_licne_podatke');
    Route::post('/promeni_lozinku', 'klijentKorisnikController@promeni_lozinku');
});

//-------------------------------------------------------------------------------------

//---------ADMIN RUTE POCETAK----------------
Route::middleware(['admin'])->group(function () {
    Route::get('/admin','adminController@naslovna');

    //--------------PROIZVODI---------------------
    Route::get('/admin/proizvodi','adminProizvodiController@proizvodi');

    Route::get('/admin/proizvod/{id}','adminProizvodiController@proizvod');


    Route::post('/admin/sacuvajProizvod/{id}','adminProizvodiController@sacuvaj_proizvod'); //za dodavanje novog ili izmene

    Route::post('/admin/obrisiProizvod/{id}' , 'adminProizvodiController@obrisi_proizvod');

    Route::post('/admin/restaurirajProizvod/{id}' , 'adminProizvodiController@restauriraj_proizvod');

    Route::post('/admin/uploadSlike' , 'adminProizvodiController@upload_slike');

    Route::post('/admin/obrisiUploadSlike' , 'adminProizvodiController@obrisi_upload_slike');

    //--------------DOBAVLJAČI---------------------
    Route::get('/admin/dobavljaci','adminDobavljaciController@dobavljaci');

    Route::get('/admin/dobavljac/{id}','adminDobavljaciController@dobavljac');

    Route::post('/admin/sacuvajDobavljaca/{id}','adminDobavljaciController@sacuvaj_dobavljaca');

    Route::post('/admin/obrisiDobavljaca/{id}','adminDobavljaciController@obrisi_dobavljaca');

    Route::post('/admin/restaurirajDobavljaca/{id}','adminDobavljaciController@restauriraj_dobavljaca');
    //--------------BRENDOVI---------------------
    Route::get('/admin/brendovi','adminBrendoviController@brendovi');

    Route::get('/admin/brend/{id}','adminBrendoviController@brend');

    Route::post('/admin/sacuvajBrend/{id}','adminBrendoviController@sacuvaj_brend');

    Route::post('/admin/brend/uploadSlike','adminBrendoviController@upload_slike');

    Route::post('/admin/brend/obrisiUploadSlike','adminBrendoviController@obrisi_upload_slike');

    Route::post('/admin/obrisiBrend/{id}','adminBrendoviController@obrisi_brend');

    Route::post('/admin/restaurirajBrend/{id}','adminBrendoviController@restauriraj_brend');

    //--------------KATEGORIJE---------------------
    Route::get('/admin/kategorije','adminKategorijeController@kategorije');

    Route::get('/admin/kategorija/{id}','adminKategorijeController@kategorija');

    Route::post('/admin/sacuvajKategoriju/{id}','adminKategorijeController@sacuvaj_kategoriju');

    Route::post('/admin/kategorija/uploadSlike','adminKategorijeController@upload_slike');

    Route::post('/admin/kategorija/obrisiUploadSlike','adminKategorijeController@obrisi_upload_slike');

    Route::post('/admin/obrisiKategoriju/{id}','adminKategorijeController@obrisi_kategoriju');

    Route::post('/admin/restaurirajKategoriju/{id}','adminKategorijeController@restauriraj_kategoriju');

    //--------------OPCIJE---------------------
    Route::get('/admin/opcije','adminOpcijeController@opcije');

    Route::get('/admin/opcija/{id}','adminOpcijeController@opcija');

    Route::post('/admin/sacuvajOpciju/{id}','adminOpcijeController@sacuvaj_opciju');

    Route::post('/admin/obrisiOpciju/{id}','adminOpcijeController@obrisi_opciju');

    Route::post('/admin/restaurirajOpciju/{id}','adminOpcijeController@restauriraj_opciju');

    //--------------SPECIFIKACIJE---------------------
    Route::get('/admin/specifikacije','adminSpecifikacijeController@specifikacije');

    Route::get('/admin/specifikacija/{id}','adminSpecifikacijeController@specifikacija');

    Route::post('/admin/sacuvajSpecifikaciju/{id}','adminSpecifikacijeController@sacuvaj_specifikaciju');

    Route::post('/admin/obrisiSpecifikaciju/{id}','adminSpecifikacijeController@obrisi_specifikaciju');

    Route::post('/admin/restaurirajSpecifikaciju/{id}','adminSpecifikacijeController@restauriraj_specifikaciju');

    //--------------TAGOVI---------------------
    Route::get('/admin/tagovi','adminTagoviController@tagovi');

    Route::get('/admin/tag/{id}','adminTagoviController@tag');

    Route::post('/admin/sacuvajTag/{id}','adminTagoviController@sacuvaj_tag');

    Route::post('/admin/obrisiTag/{id}','adminTagoviController@obrisi_tag');

    Route::post('/admin/restaurirajTag/{id}','adminTagoviController@restauriraj_tag');

    //--------------PORUDZBINE---------------------
    Route::get('/admin/porudzbine','adminPorudzbineController@porudzbine');

    Route::get('/admin/porudzbina/{id}','adminPorudzbineController@porudzbina');

    Route::get('/admin/fakture','adminPorudzbineController@fakture');

    Route::get('/admin/faktura/{id}','adminPorudzbineController@faktura');

    Route::post('/admin/statusPorudzbine/{id}/{status}','adminPorudzbineController@statusPorudzbine');

    //--------------KORISNICI---------------------
    Route::get('/admin/korisnici','adminKorisniciController@korisnici');

    Route::get('/admin/korisnik/{id}','adminKorisniciController@korisnik');

    Route::post('/admin/blokirajKorisnika/{id}','adminKorisniciController@blokiraj_korisnika');

    Route::post('/admin/odblokirajKorisnika/{id}','adminKorisniciController@odblokiraj_korisnika');

    Route::post('/admin/sacuvajKorisnika/{id}','adminKorisniciController@sacuvaj_korisnika');

    //--------------IZVESTAJI---------------------
    Route::get('/admin/izvestaji','adminIzvestajiController@izvestaji');

    Route::get('/admin/izvestaj','adminIzvestajiController@dohvatiIzvestaj');

    Route::post('/admin/izvestajiPost','adminIzvestajiController@izvestajiPost');


    //--------------KUPONI---------------------
    Route::get('/admin/kuponi','adminPopustiController@kuponi');

    Route::get('/admin/kupon/{id}','adminPopustiController@kupon');

    Route::post('/admin/sacuvajKupon/{id}','adminPopustiController@sacuvaj_kupon');

    Route::post('/admin/obrisiKupon/{id}','adminPopustiController@obrisi_kupon');

    Route::post('/admin/restaurirajKupon/{id}','adminPopustiController@restauriraj_kupon');

    Route::post('/admin/aktivirajKupon/{id}','adminPopustiController@aktiviraj_kupon');

    Route::post('/admin/deaktivirajKupon/{id}','adminPopustiController@deaktiviraj_kupon');

    //--------------VAUČERI---------------------
    Route::get('/admin/vauceri','adminPopustiController@vauceri');

    Route::get('/admin/vaucer/{id}','adminPopustiController@vaucer');

    Route::post('/admin/sacuvajVaucer/{id}','adminPopustiController@sacuvaj_vaucer');

    Route::post('/admin/obrisiVaucer/{id}','adminPopustiController@obrisi_vaucer');

    Route::post('/admin/restaurirajVaucer/{id}','adminPopustiController@restauriraj_vaucer');

    Route::post('/admin/iskoristiVaucer/{id}','adminPopustiController@iskoristi_vaucer');

    //--------------BLOG---------------------
    Route::get('/admin/blog','adminBlogController@blog');

    Route::get('/admin/clanak/{id}','adminBlogController@clanak');

    Route::post('/admin/sacuvajClanak/{id}','adminBlogController@sacuvaj');

    Route::post('/admin/obrisiClanak/{id}','adminBlogController@obrisi');

    Route::post('/admin/restaurirajClanak/{id}','adminBlogController@restauriraj');

    Route::post('/admin/clanak/uploadSlike','adminBlogController@upload_slike');

    Route::post('/admin/clanak/obrisiUploadSlike','adminBlogController@obrisi_upload_slike');

    //--------------REZERVACIJE---------------------
    Route::get('/admin/rezervacije','adminRezervacijeController@rezervacije');

    Route::get('/admin/rezervacija/{id}','adminRezervacijeController@rezervacija');

    Route::post('/admin/sacuvajRezervaciju/{id}','adminRezervacijeController@sacuvaj_rezervaciju');
    Route::post('/admin/obrisiRezervaciju/{id}','adminRezervacijeController@obrisi_rezervaciju');
    Route::post('/admin/restaurirajRezervaciju/{id}','adminRezervacijeController@restauriraj_rezervaciju');
    Route::post('/admin/potvrdiRezervaciju/{id}','adminRezervacijeController@potvrdi_rezervaciju');
    Route::post('/admin/statusRezervacije/{id}/{status}','adminRezervacijeController@status_rezervacije');

    //--------------USLUGE---------------------
    Route::get('/admin/usluga/{id}','adminUslugeController@usluga');
    Route::get('/admin/usluge','adminUslugeController@usluge');
    Route::post('/admin/sacuvajUslugu/{id}','adminUslugeController@sacuvaj_uslugu');

    Route::post('/admin/obrisiUslugu/{id}','adminUslugeController@obrisi_uslugu');

    Route::post('/admin/restaurirajUslugu/{id}','adminUslugeController@restauriraj_uslugu');

});
//---------ADMIN RUTE KRAJ----------------
Auth::routes();

Route::get('/redirect/{provider}', 'Auth\SocialAuthController@redirect');
Route::get('/callback/{provider}', 'Auth\SocialAuthController@callback');
